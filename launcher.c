// This excutable must be setuid root
// Arguments:
//   * The login
//   * .profile content
// Create a process group named SANSOORI_UID and make it killable by SANSOORI
// Limit to 100MB of memory and 10 concurrent threads.

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#define NAME(X, Y) snprintf(filename, sizeof(filename), X, Y)
#define NAME3(X, Y, Z) snprintf(filename, sizeof(filename), X, Y, Z)

int main(int argc, char **argv)
{
    char filename[80];
    struct passwd *passwd;

    //////////////////////////////////////////////////////////////////////////
    // Check if the users are fine
    //////////////////////////////////////////////////////////////////////////
    if (getuid() != SANSOORI_UID)
        return 1; // Not the Sansoori user
    //if (strlen(argv[1]) != 8)
    //    return 2; // Not a student login
    //if ( argv[1][2] <= '0' || argv[1][2] >= '9' )
    //    return 3; // Not a student login
    passwd = getpwnam(argv[1]);
    if ( ! passwd )
        return 4;

    //////////////////////////////////////////////////////////////////////////
    // Become PTY process leader
    //////////////////////////////////////////////////////////////////////////
    if (setsid() == -1)
        perror("setsid");
    if (ioctl(1, TIOCSCTTY, 0))
        perror("ioctl");

    //////////////////////////////////////////////////////////////////////////
    // Create control group to limit resources usage and allow group killing
    //////////////////////////////////////////////////////////////////////////
    NAME("/sys/fs/cgroup/SANSOORI_%d", passwd->pw_uid);
    if ( mkdir(filename, 0555) )
        {
            rmdir(filename); // Clear stats
            mkdir(filename, 0555);
        }

    // Add this processus to the control group
    NAME("/sys/fs/cgroup/SANSOORI_%d/cgroup.procs", passwd->pw_uid);
    FILE *cgroup_procs = fopen(filename, "w");
    if ( cgroup_procs == NULL )
        {
            perror(filename);
            //return 5;
        }
    else
        {
            fprintf(cgroup_procs, "%d\n", getpid());
            fclose(cgroup_procs);
        }

    // Allow the real UID to kill the control group
    NAME("/sys/fs/cgroup/SANSOORI_%d/cgroup.kill", passwd->pw_uid);
    if(chown(filename, getuid(), getgid()))
         {
            // perror(filename);
            //return 6;
        }

    //////////////////////////////////////////////////////////////////////////
    // Limit resource usage
    //////////////////////////////////////////////////////////////////////////

    if (strcmp(argv[1], USER) != 0)
        {
            struct rlimit cpu = {2, 2};
            struct rlimit data = {100*1024*1024, 100*1024*1024};
            struct rlimit nproc = {21, 21};
            setrlimit(RLIMIT_CPU, &cpu);
            setrlimit(RLIMIT_DATA, &data);
            setrlimit(RLIMIT_NPROC, &nproc);
        }

    //////////////////////////////////////////////////////////////////////////
    // Drop root rights
    //////////////////////////////////////////////////////////////////////////
    if(setregid(passwd->pw_gid, passwd->pw_gid))
        {
            perror("setregid");
            return 7;
        }
    if(setreuid(passwd->pw_uid, passwd->pw_uid))
        {
            perror("setreuid");
            return 8;
        }
    //////////////////////////////////////////////////////////////////////////
    // Configure home directory and environment
    //////////////////////////////////////////////////////////////////////////
    NAME3("rm -rf %s/%s", HOME, argv[1]);
    system(filename); // May fail if too many user processes
    NAME3("%s/%s", HOME, argv[1]);
    if (mkdir(filename, 0755))
        {
            perror("mkdir");
            return 9;
        }
    if(chdir(filename))
        {
            perror("chdir");
            return 10;
        }
    FILE *profile = fopen(".profile", "w");
    fwrite(argv[2], 1, strlen(argv[2]), profile);
    fclose(profile);

    // Set minimal environment
    setenv("HOME", filename, 1);
    setenv("TERM", "xterm", 1);

    //////////////////////////////////////////////////////////////////////////
    // Run the real job
    //////////////////////////////////////////////////////////////////////////
    execl("/bin/bash", "bash", "-l", NULL);
    perror("/bin/bash");
}