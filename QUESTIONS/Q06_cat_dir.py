# Competence needed to answer this question
NEEDS = ['CMD_cat', 'CMD_ls']
# Competence automaticaly given on good answer
PROVIDES = ['FS_navigate']

INIT = """
echo "Préparer mes cours pour demain" >todo
"""

def run(session):
    '''Messages d'erreur'''
    dirname = session.choice(('/etc', '/tmp', '/var', '/usr'))
    session.ask(f"Affichez le contenu de <path>{dirname}</path> avec la commande <cmd>cat</cmd>")
    for _screen in session.until(screen_contains='Is a directory'):
        pass
    session.print("""La commande <cmd>cat</cmd> a refusé de fonctionner car
    le chemin indiqué mène à un répertoire.

    Visuellement il n'y a aucun moyen de savoir si ce qui est affiché est
    un contenu de fichier ou un message d'erreur.
    Mais dans ce cas, il est très improbable que cela soit un contenu de fichier.
    """)
    session.course('3:Syntaxe',
        """Quand la syntaxe est bonne et que la commande fonctionne,
        alors rien n'est affiché pour le dire.

        Quand il y a une erreur, elle est affichée la ligne au dessous de la commande.
        """)
