from Q06_elsewhere import INIT, get_choices # pylint: disable=unused-import

# Competence needed to answer this question
NEEDS = ['PROC_currentDir', 'CMD_cd', 'SYNTAX_argument', 'FS_path']
# Competence automaticaly given on good answer
PROVIDES = ['FS_path_relative']
# Dictionnary of common messages, shared between all the questions
WORDS = {
    '<SPACE>':
        "Une ligne de commande est comme une phrase, il faut séparer les mots.",
    '<CASE>':
        "Les minuscules et majuscules sont importantes."
}

def run(session):
    '''Explique le chemin relatif'''
    dirname, _expect = session.choice(get_choices(session, relative=True))
    session.print('''
Un <def>chemin relatif</def> est un chemin qui ne commence pas par le caractère <path>/</path>
il commence à l'endroit où l'on se trouve, donc le <def>répertoire courant</def> (l'endroit où l'on est).''')
    session.ask('''
Lancez la commande qui affiche l'endroit où vous êtes,
c'est-à-dire le <def>chemin absolu</def> vers votre <def>répertoire courant</def>.
''')
    session.expect('CMD_pwd')
    session.press_enter(screen_contains='$ pwd\n/')

    session.ask(f'''Changez votre <def>répertoire courant</def>,
en utilisant le <def>chemin relatif</def> : <path>{dirname}</path>
pour descendre dans ce répertoire.
''')
    session.expect('CMD_cd')
    for screen in session.until(screen_contains=f'$ cd {dirname}\n$'):
        session.transient("<CASE>", dirname in screen.upper() and dirname not in screen)
        if 'cd /' in screen:
            session.cheat("Vous avez utilisé un chemin absolu et non un chemin relatif !")

    session.ask('Affichez à nouveau le chemin vers le <def>répertoire courant</def>.')
    session.wait(screen_contains=f'{session.home}/{dirname}')
    session.print("<important>Vous voyez qu'il s'est allongé !</important>")
