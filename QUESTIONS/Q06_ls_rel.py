from Q06_elsewhere import INIT, get_choices # pylint: disable=unused-import

# Competence needed to answer this question
NEEDS = ['CMD_ls', 'FS_path', 'SYNTAX_argument']
# Competence automaticaly given on good answer
PROVIDES = ['FS_path_relative']

def run(session):
    '''Explique le chemin relatif'''
    dirname, expect = session.choice(get_choices(session, relative=True))
    session.course('2:Chemins', '''
Un <def>chemin relatif</def> est un chemin vers une entité (fichier, répertoire…)
qui ne commence pas par le caractère <path>/</path>.

Le <def>chemin relatif</def> commence à l'endroit où l'on se trouve.
''')
    session.ask('''Lancez la commande <cmd>ls</cmd> pour voir
les fichiers, répertoires... se trouvant dans l'endroit
où vous êtes actuellement (votre <def>répertoire courant</def>).
''')
    session.expect('CMD_ls')

    session.print('La commande <cmd>ls</cmd> peut prendre un chemin comme <def>argument</def>.')
    session.ask(f'''
Affichez le contenu du <def>répertoire</def> indiqué par le <def>chemin relatif</def> <path>{dirname}</path>
<b>sans changer votre <def>répertoire courant</def></b>.''')
    for screen in session.until(screen_contains_one=expect):
        if 'cd' in screen:
            session.cheat("Vous ne deviez pas utiliser la commande <cmd>cd</cmd>")
        session.transient("<SPACE>", f'$ ls{dirname[0]}' in screen)
        session.transient("<CASE>", dirname in screen.upper() and dirname not in screen)
