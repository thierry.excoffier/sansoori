# Competence needed to answer this question
NEEDS = ['CMD_ls_l', 'CMD_ls_a']
# Competence automaticaly given on good answer
PROVIDES = ['SYNTAX_option_concat']

INIT = """
mkdir COURS LOISIRS .ssh
echo "Ceci est un fichier ne contenant pas grand chose." >README
touch --date '2002-01-01 00:00:01' *
touch --date '2001-01-02 00:00:01' README
touch --date '2003-01-03 00:00:01' .
"""

WORDS = {
    '<CONCAT>':
        'Pour gagner du temps, on ne tape pas <opt>-X</opt> <opt>-Y</opt> mais <opt>-XY</opt>'}

def run(session):
    '''Options de commande'''
    session.ask("Listez le contenu du répertoire courant en incluant les entités cachées.")
    session.expect('CMD_ls')
    for _screen in session.until(screen_contains='.ssh'):
        if session.tip(10, "Vous devez lancer <cmd>ls</cmd> avec l'option «<opt>a</opt>»"):
            session.mul('CMD_ls_a', 0.9)

    session.ask("Lister le contenu du répertoire courant avec tous les détails.")
    for _screen in session.until(screen_contains='2001 README'):
        if session.tip(20, "Vous devez lancer <cmd>ls</cmd> avec l'option «<opt>l</opt>»"):
            session.mul('CMD_ls_l', 0.9)

    session.course('3:Syntaxe', """
Les options sur une seule lettre peuvent être fusionnées.

Vous écrivez <cmd>-XY</cmd> ou <cmd>-YX</cmd> au lieu de <cmd>-X -Y</cmd>
afin de gagner du temps car cela fait moins de caractères à taper.
""")
    session.ask("""
Lancez la commande <cmd>ls</cmd> avec les options <opt>l</opt> et <opt>a</opt>.
""")

    for command in session.until(command_startswith=('ls -al', 'ls -la')):
        session.transient('<CONCAT>', '-a -l' in command or '-l -a' in command)
        session.transient("""
<tip>Si vous écrivez <opt>-X-Y</opt> cela demande l'option <opt>-</opt>.
Ce n'est certainement pas ce que vous voulez.
""",
            '-l-a' in command or '-a-l' in command
        )
    session.press_enter(screen_contains='2003 .')
    session.print("\nVous voyez maintenant les détails des fichiers cachés.")
