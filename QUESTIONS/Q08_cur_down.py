# Competence needed to answer this question
NEEDS = ['KBD_cursor_up']
# Competence automaticaly given on good answer
PROVIDES = ['KBD_cursor_down']

INIT = """
echo 'echo "Première commande"
cd /usr/share
ls /etc
ls /usr
pwd
ls /usr/include' >>$HOME/.bash_history
history -n
"""

WORDS = { '<NOEDIT>': 'Il ne faut pas retaper la commande !' }

def run(session):
    '''Explique curseur bas'''
    session.ask("""Faites afficher les commandes précédentes de l'historique
jusqu'à retrouver la commande <cmd>cd</cmd>""")
    for command in session.until(command_is='cd /usr/share'):
        if command in ('c', 'cd', 'cd ', 'cd /'):
            session.cheat("<NOEDIT>")
        session.tip(10,
            "Utilisez la flèche vers le haut du clavier pour remonter dans l'historique.")
    session.course("2:Chemins/Interaction",
        """La touche <key>↓</key> permet de revenir sur des commandes plus récentes
        si vous avez dépassé la commande qui vous intéressait.""")
    session.ask("Utilisez le curseur vers le bas pour retrouver <cmd>pwd</cmd>")
    for command in session.until(command_is='pwd'):
        if command in ('p', 'pw'):
            session.cheat("<NOEDIT>")
