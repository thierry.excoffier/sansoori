# Competence needed to answer this question
NEEDS = []
# Competence automaticaly given on good answer
PROVIDES = ['SYNTAX_enter']

def run(session):
    '''Le «prompt» et la touche «Entrée»'''
    session.print('''Bienvenue sur Sansoori.

Le cours, les questions et l'aide sont indiquées ici.

Un <box class="box" closed="1"
     style="margin-left: 0px;display: inline-block;vertical-align: bottom;"></box>
indique qu'un message est disponible,
pour le lire appuyez sur la touche <key>Ctrl</key> (contrôle).
''')
    session.sleep(2)
    session.print('''
<important>Prenez la souris et placez-la dans un endroit inaccessible, elle ne sert à rien.</important>

En effet vous n'avez besoin que du clavier pour répondre.
La main gauche s'occupe de la partie gauche du clavier et
la droite de la partie droite et des touches de déplacement de curseur.
''')
    session.sleep(5)
    session.ask('''
Appuyez une fois sur la touche <key>Entrée</key> du clavier,
ou <key><lang>Enter</lang></key> si c'est un clavier anglais.''')
    for _screen in session.until(screen_contains='$\n$ '):
        session.tip(30, """<key>Entrée</key> est la grosse touche
        sur la partie droite du clavier alphabétique,
        celle du clavier numérique est trop loin et fait perdre du temps.""")

    session.course('0:Introduction', '''
Le <def>shell</def> est un langage qui permet de communiquer avec la machine.

Le <def>shell</def> vous indique qu'il est prêt à recevoir vos commandes
en affichant le <lang><def>prompt</def></lang> ou <def>invite de commande</def> en français.

Le <lang><def>prompt</def></lang> est le «<screen>$ </screen>»
qui est à gauche du curseur (le rectangle blanc).

Le <def>shell</def> exécute les commandes quand vous appuyez sur la touche <key>Entrée</key>.''', wait=False)

    session.ask("""Pour passer à la question suivante, appuyez sur <key>²</key> (puissance 2)
    juste à gauche de <key>&amp; 1</key> et choisisser le premier item du menu
    en refaisant <key>²</key> (puissance 2).

    Si votre clavier est incomplet, utilisez l'une des touches suivantes à la place :
    <key>Command</key> <key>Windows</key> <key>Meta</key>.
    """)
