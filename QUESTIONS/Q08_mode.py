# Competence needed to answer this question
NEEDS = ['CMD_ls_l', 'PROC_login']
# Competence automaticaly given on good answer
PROVIDES = ['FS_mode']

def run(session):
    '''Exécutable'''
    session.ask("Affichez les détails sur l'entité <path>/bin/rm</path>")
    session.expect('CMD_ls')
    for screen in session.until(screen_contains='rwx'):
        if '-' not in screen and session.tip(10, "Vous devez indiquer une option."):
            session.mul('SYNTAX_option_courte', 0.9)
        if '-l' not in screen \
                and session.tip(50, "Vous devez utiliser l'option <opt>l</opt>"):
            session.mul('CMD_ls_l', 0.9)
        if '/bin/rm' not in screen and session.tip(20, "Vous oubliez <path>/bin/rm</path>"):
            session.mul('SYNTAX_option_courte', 0.9)
        if session.transient('Vous avez oublié le tiret avant le «l»', 'ls l' in screen):
            session.mul('SYNTAX_option_courte', 0.9)

    session.course('1:Entités/Mode',
        '''Si la commande <cmd>ls -l</cmd> affiche :<br>
<tt>-rwxr-xr-x 1 root root 138208 Feb  7  2022 /bin/rm</tt><br>
Cela se lit de la façon suivante :
<table>
<tr><td><tt>-</tt>       <td> Un <def>fichier texte</def>
                               ('<tt>d</tt>' indique un répertoire)
<tr><td><tt>rwx</tt>     <td> Le <def>propriétaire</def> du fichier à le droit de :<br>
                              «r» (<lang>read</lang>) lire,<br>
                              «w» (<lang>write</lang>) écrire,<br>
                              «x» (<lang>eXecute</lang>) exécuter le fichier.
<tr><td><tt>r-x</tt>     <td> Les membres du <def>groupe d'utilisateur</def>
                              n'ont pas le droit d'écrire dans le fichier.
<tr><td><tt>r-x</tt>     <td> Les autres utilisateurs non plus.
<tr><td><tt>1</tt>       <td> Nombre de chemins menant à ce fichier.</tr>
<tr><td><tt>root</tt>    <td> <def>login</def> du propriétaire du fichier.</tr>
<tr><td><tt>root</tt>    <td> Nom du groupe propriétaire du fichier.</tr>
<tr><td><tt>138208</tt>  <td> Taille du fichier en octet.</tr>
<tr><td><tt>Feb  7</tt>  <td> Date de dernière modification du fichier.</tr>
<tr><td><tt>/bin/rm</tt> <td> Chemin vers le fichier.</tr>
</table>
'<tt>rwxr-xr-x</tt>' est appelé <def>mode</def> du fichier.
''')
