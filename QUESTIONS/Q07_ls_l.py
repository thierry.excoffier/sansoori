# Competence needed to answer this question
NEEDS = ['FS_path_absolute', 'SYNTAX_argument']
# Competence automaticaly given on good answer
PROVIDES = ['SYNTAX_option_courte', 'CMD_ls_l']

INIT = """
mkdir COURS LOISIRS
echo "Une coquille vide." >README
"""

def run(session):
    '''Options de commande'''
    name = session.choice(('/etc', '/usr', '/tmp'))
    session.course('3:Syntaxe', """
Les <def>options courtes</def> permettent de changer le comportement des commandes.
Elles sont placées <b>avant</b> les arguments car elles s'appliquent à eux.
Une option est un argument préfixé par le caractère «-».
""")
    session.ask(f"""
Faites afficher les entités contenues dans le répertoire <path>{name}</path>
en indiquant l'option «<opt>l</opt>» (la lettre, pas le chiffre).
""")
    session.expect('CMD_ls')

    for command in session.until(command_startswith='ls -l'):
        session.transient("Vous avez oublié le tiret avant l'option <opt>l</opt>",
                          'ls l' in command)
        session.transient("Il faut indiquer l'option juste après <cmd>ls</cmd>",
                          'ls ' + name in command)
        session.tip(30, "Vous devez maintenant indiquer l'option «<opt>l</opt>»")

    for command in session.until(command_startswith='ls -l ' + name):
        if session.transient("Il faut séparer les arguments entre-eux par des espaces.",
                             command == 'ls -l' + name):
            session.mul('SYNTAX_argument', 0.9)
        session.transient("Vous avez cassé le début de la commande qui était juste, remettez-le.",
                          'ls -l' not in command)
        session.tip(20,
                    "Votre ligne contient des choses en trop."
                    if name in command
                    else f"Votre ligne devrait contenir <path>{name}</path>"
                   )
    session.press_enter(next_command_is='')
    session.print("""
L'option «<opt>l</opt>» de <cmd>ls</cmd> permet d'afficher plein d'informations.
Le type du fichier, les droits d'accès, le propriétaire,
la taille en octets, la date de dernière modification et le nom.
""")
