from Q06_elsewhere import INIT, get_choices # pylint: disable=unused-import

# Competence needed to answer this question
NEEDS = ['FS_path_relative', 'FS_path_absolute']
# Competence automaticaly given on good answer
PROVIDES = ['KBD_cursor_up']

def run(session):
    '''Explique curseur haut'''
    dirname, _expect = session.choice(get_choices(session, relative=True, absolute=True))
    session.ask(f'Allez dans le répertoire <path>{dirname}</path>.')
    session.expect('CMD_cd')
    session.wait(last_command_startswith=f'cd {dirname}')

    session.ask('Affichez le chemin vers le répertoire courant.')
    session.expect('CMD_pwd')

    session.ask('Allez dans le répertoire <path>/bin</path> (chemin absolu).')
    session.expect('CMD_cd')
    session.wait(last_command_is='cd /bin')

    session.course("2:Chemins/Interaction",
        """La touche <key>↑</key> permet de reprendre des commandes que vous avez tapé
        dans le passé.""")
    session.print("""
Maintenant on veut refaire la commande <cmd>pwd</cmd> sans la retaper.""")
    session.ask("""
Tapez 2 fois sur la touche curseur vers le haut <key>↑</key>
pour retourner sur la commande <cmd>pwd</cmd>
puis appuyez sur <key>Entrée</key>.

<important>Si vous retapez p w d vous avez perdu !</important>""")
    for command in session.until(command_is='pwd'):
        if command in ('p', 'pw'):
            session.fail("Perdu !")

    session.ask(f"""Appuyez sur <key>↑</key> pour revenir
sur la commande <cmd>cd {dirname}</cmd>""")
    session.wait(command_startswith=f"cd {dirname}")
