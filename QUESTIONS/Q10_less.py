NEEDS = ['FS_path_absolute', 'CMD_cat']
PROVIDES = ['CMD_less']

def run(session):
    '''less'''
    dirname, filename = ('/etc', 'mime.types')
    session.course("4:Commandes/less",
        """La commande <cmd>less</cmd> permet d'afficher le contenu d'un fichier
        page par page à partir du début et de naviguer à l'intérieur en utilisant le clavier.
        """)
    session.ask(f'''Affichez avec la commande <cmd>less</cmd> le fichier
        <path>{filename}</path> se trouvant dans le répertoire <path>{dirname}</path>.''')

    session.expect('CMD_less')
    for command in session.until(command_startswith=f'less {dirname}/{filename}'):
        session.tip(30, f"L'argument doit être <path>{dirname}/{filename}</path>")

    session.wait(screen_contains=f'\n{dirname}/{filename}')
    session.print("""Le <def>prompt</def> n'est pas revenu car la commande
    <cmd>less</cmd> est en train de fonctionner.
    Ce que vous tapé est interprété par le processus <cmd>less</cmd>.
    
    Ce que vous voyez ce sont les premières lignes du fichier.
    """)
    session.course("4:Commandes/less",
        "<key>h</key> comme <lang>help</lang> permet d'afficher une page d'explication.")
    session.ask("Appuyez sur <key>h</key>, pas besoin d'appuyer sur <key>Entrée</key>")
    session.wait(screen_contains='^E')

    session.print("En bas il y a des explications concernant l'aide.")
    session.ask("Appuyez plusieurs fois sur <key>Entrée</key> pour voir la suite de l'aide.")
    session.wait(screen_contains='}')

    session.course("4:Commandes/less",
        """La touche <key>Entrée</key> permet de voir la ligne suivante
        et la touche <key>Espace</key> la page suivante.""")
    session.ask("Appuyez plusieurs fois sur <key>Espace</key> pour voir la suite de l'aide.")
    session.wait(screen_contains_one=('JUMPING', 'CHANGING', 'OPTIONS'))

    session.ask("""Appuyez sur la touche <key>Fin</key> pour voir la fin de l'aide.
        ou le caractère <cmd>&gt;</cmd> si votre clavier est incomplet.""")
    session.wait(screen_contains="LINE EDITING")

    session.course("4:Commandes/less",
        "<key>q</key> comme <lang>quit</lang> permet de quitter l'application.")
    session.ask("Appuyez sur la touche <key>q</key> pour quitter l'aide.")
    session.wait(screen_contains_one="#/")

    session.ask("Appuyez sur la touche <key>q</key> pour quitter l'application.")
    session.wait(screen_contains=f'less {dirname}/{filename}')
