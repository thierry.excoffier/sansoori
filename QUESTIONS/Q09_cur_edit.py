# Competence needed to answer this question
NEEDS = ['KBD_cursor_down']
# Competence automaticaly given on good answer
PROVIDES = ['KBD_cursor_edit']

INIT = """
echo 'echo "Première commande"
ls /usr/share/grib/default/grub
cd /tmp
date
' >>$HOME/.bash_history
history -n
"""


def run(session):
    '''Explique édition commande'''
    session.ask("Remontez dans l'historique des commandes jusqu'à la commande <cmd>ls /usr/...</cmd>.")
    for command in session.until(command_is='ls /usr/share/grib/default/grub'):
        session.tip(10,
            "Utilisez la flèche vers le haut du clavier pour remonter dans l'historique.")

    session.ask("""
Utilisez le curseur vers la gauche pour remplacer <path>grib</path> par <path>grub</path>
Vous pouvez enlever le 'i' de 2 manières :
<ul>
   <li> Vous allez sur la lettre 'b' puis tapez <key>Backspace</key> (espace arrière)
   <li> ou bien vous allez sur la lettre 'i' puis tapez <key>Suppr</key>
</ul>""")
    for command in session.until(command_is='ls /usr/share/grb/default/grub'):
        if command == 'ls /usr/share/grub/default/grub':
            return
        if 'ls /usr/share' in command and ('grub' not in command or 'default' not in command):
            session.cheat("Vous ne devez pas effacer la fin de la ligne !")

    session.ask("""
Tapez 'u' pour corriger puis directement <key>Entrée</key>
sans avoir besoin d'aller en fin de la ligne.""")
    for command in session.until(command_is='ls /usr/share/grub/default/grub'):
        pass

    session.course("2:Chemins/Interaction",
        """Les touches <key>←</key> et <key>→</key> permettent de modifier
        la commande courante ou une commande faite dans le passé.

        Cela permet donc de gagner un temps précieux en ne retapant pas les commandes.
        """)
