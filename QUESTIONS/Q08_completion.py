import random

# Competence needed to answer this question
NEEDS = ['KBD_completion_full', 'CMD_cat', 'CMD_ls']
PROVIDES = ['KBD_completion_middle']

def INIT(questions):
    questions.random_filename = f'{random.randint(0, 1<<100):x}-'
    questions.random_filename2 = f'{random.randint(0, 1<<100):x}'
    return f'''echo "Facile aussi !" >{questions.random_filename}{questions.random_filename2}
    for I in 1 2 3 4 5 6 7 8 9 a b c d e f ;
    do
    if [ $I != {questions.random_filename2[0]} ]
    then
    echo "Pas celui-ci" >{questions.random_filename}$I.txt
    fi
    done
    '''

def run(session):
    '''Complétion en 2 partie'''
    session.ask("Affichez le contenu du répertoire courant.")
    filename = session.questions.random_filename
    filename2 = session.questions.random_filename2
    session.expect("CMD_ls")
    for _screen in session.until(screen_contains=filename):
        pass
    session.ask("Affichez le contenu du fichier qui a le nom le plus long.")
    session.course("2:Chemins/Complétion",
        """La complétion s'arrête quand il y a un choix à faire parmi plusieurs possibilités.
        Vous tapez alors suffisemment de caractères pour éliminer les choix
        qui ne vous intéressent pas et appuyez à nouveau sur <key>Tabulation</key>.
        """)
    session.expect('CMD_cat')
    session.print("Tapez la première lettre du nom.")
    for command in session.until(command_startswith=f'cat {filename}'):
        if command.startswith(f'cat {filename[0]}'):
            session.tip(40, "Appuyez sur la touche <key>Tabulation</key> pour compléter.")
    for command in session.until(command_startswith=f'cat {filename}{filename2}'):
        if not command.startswith(f'cat {filename}{filename2[1]}'):
            session.tip(5, """Tapez maintenant la lettre permettant de choisir le bon fichier
    et appuyez à nouveau sur <key>Tabulation</key>.""")
    session.press_enter(screen_contains="Facile aussi !")
