import random

# Competence needed to answer this question
NEEDS = ['FS_path_relative', 'FS_path_absolute', 'CMD_cat', 'CMD_ls']
PROVIDES = ['KBD_completion_full']

def INIT(questions):
    questions.random_filename = f'{random.randint(0, 1<<200):x}'
    return f'''echo "Facile !" >{questions.random_filename}
    for I in 1 2 3 4 5 6 7 8 9 a b c d e f ;
    do
    if [ $I != {questions.random_filename[0]} ]
    then
    echo "Pas celui-ci" >$I.txt
    fi
    done
    '''

def run(session):
    '''Complétion simple'''
    session.ask("Affichez le contenu du répertoire courant.")
    filename = session.questions.random_filename
    for _screen in session.until(screen_contains=filename):
        pass
    session.course("2:Chemins/Complétion",
        """<def>Complétion</def> : la touche <key>Tabulation</key>
        (le symbole sur la touche peut être <key>⭾</key> <key>⇥</key> <key>Tab</key>)
        complète contextuellement ce que vous êtes en train de taper.""")
    session.course("2:Chemins/Complétion",
        """Si une seule complétion est possible alors cela va
        directement la taper à votre place.""")
    session.ask("Affichez le contenu du fichier qui a un nom très long.")
    session.expect('CMD_cat')
    session.ask("Tapez la première lettre du nom.")
    for command in session.until(command_startswith=f'cat {filename}'):
        if command.startswith(f'cat {filename[0]}'):
            session.tip(5, "Appuyez sur la touche <key>Tabulation</key> pour compléter.")
    session.press_enter(screen_contains="Facile !")
