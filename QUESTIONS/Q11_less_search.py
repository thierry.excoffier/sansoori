NEEDS = ['CMD_less']
PROVIDES = ['CMD_less_search']
INIT = """
cd /etc/dictionaries-common
"""

def run(session):
    '''Explique /'''
    session.ask('Affichez le chemin vers le répertoire courant.')
    session.expect('CMD_pwd')
    session.press_enter(screen_contains='/etc/dictionaries-common')

    session.ask('Lister les noms des entitées contenues dans répertoire courant.')
    session.expect('CMD_ls')
    session.press_enter(screen_contains='words')

    session.ask('Affichez le contenu du fichier <path>words</path> <b>page par page</b>.')
    session.expect('CMD_less')
    session.wait(screen_startswith="A\n")

    session.course("4:Commandes/less",
        """<key>/</key> permet de chercher un texte vers le bas,
        éventuellement jusqu'à la fin du fichier.

        Après avoir tapé <key>/</key> vous indiquez le texte à rechercher
        puis tapez sur <key>Entrée</key> pour lancer la recherche.
        """)
    session.ask("Cherchez le texte «earl».")
    session.wait(screen_contains="Pearl")

    session.ask("Quittez le programme <cmd>less</cmd>.")
    session.wait(screen_contains="$ less words")
