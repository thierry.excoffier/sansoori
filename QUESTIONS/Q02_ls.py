# Competence needed to answer this question
NEEDS = ['PROC_currentDir']
# Competence automaticaly given on good answer
PROVIDES = ['CMD_ls']
# Current directory
INIT = "cd /"

def run(session):
    '''La commande «ls» sans paramètres'''
    session.course('4:Commandes/ls', '''
La commande <cmd>ls</cmd> (pour 'list') sans arguments affiche les noms des entités
qui sont dans le répertoire courant.

Cela peut être des noms de fichiers, de répertoires ou d'autres choses.''')
    session.ask('Lancez la commande <cmd>ls</cmd>.')
    for _command in session.until(command_is='ls'):
        session.tip(20, "Vous devez simplement taper <cmd>ls</cmd>")
    session.press_enter(screen_contains='proc')
    session.print("""
Les noms de entités sont affichés sous la forme d'un tableau.
Vous remarquerez que les noms ne contiennent pas d'espace,
il peut y en avoir mais cela complique l'écriture des commandes.

Moralité, si dans le futur vous devez échanger des fichiers avec des personnes
qui utilisent un shell, évitez d'utiliser des espaces dans les noms.
""")
