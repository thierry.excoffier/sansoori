NEEDS = ['CMD_man', 'CMD_less_search']
PROVIDES = ['CMD_man_navigate']

def run(session):
    '''Explique man'''
    session.course('4:Commandes/man',
        """La commande <cmd>man</cmd> utilise <cmd>less</cmd> pour faire l'affichage,
        vous pouvez donc utiliser toutes les commandes interactive de <cmd>less</cmd>
        pour vous déplacer dans le manuel.""")
    session.ask("Affichez la page du manuel de la commande <cmd>gcc</cmd>.")
    session.expect('CMD_man')
    session.wait(command_is="man gcc")
    session.press_enter(screen_contains="GCC(1)")

    session.ask("Cherchez «BUGS» en utilisant la recherche de mot.")
    for screen in session.until(screen_startswith="BUGS\n"):
        if 'Options' in screen:
            session.print("""Ne cherchez pas visuellement le mot,
                il y a 20000 lignes dans le manuel, vous perdez votre précieux temps.""")

    session.ask("Quittez le manuel.")
    session.wait(command_is="man gcc")