from Q09_point import INIT # pylint: disable=unused-import

# Competence needed to answer this question
NEEDS = ['FS_hidden', 'SYNTAX_option_concat']
# Competence automaticaly given on good answer
PROVIDES = ['FS_pointpoint']

from Q09_point import ls_la

def run(session):
    '''..'''
    ls_la(session)
    session.ask("Affichez le chemin absolu du répertoire courant.")
    session.expect('CMD_pwd')

    session.ask("""
Allez dans le répertoire <path>..</path> qui est le père du répertoire courant.
""")
    session.expect('CMD_cd')
    session.wait(next_command_is='')

    session.ask("""
Affichez à nouveau le chemin absolu du répertoire courant
pour vérifier qu'il a été raccourci.""")
    for _screen in session.until(screen_contains=str(session.home) + '\n'):
        pass

    session.course("2:Chemins", """
<path>..</path> est le père du répertoire le contenant,
donc <cmd>cd <path>..</path></cmd> permet de remonter dans le père
quelque soit l'endroit ou l'on se trouve (sauf à la racine).""")
