# Competence needed to answer this question
NEEDS = ['SYNTAX_enter']
# Competence automaticaly given on good answer
PROVIDES = ['CMD_pwd', 'PROC_currentDir']

WORDS = {
    '<ENTER>': "Tapez sur la touche <key>Entrée</key> pour lancer la commande.",
    '<«»>' : "Vous ne devez pas indiquer les « et » car ils font partie de l'énoncé",
    }

def run(session):
    '''Le répertoire courant et la commande «pwd»'''
    session.course('1:Entités', '''
Un <def>système de fichier</def> est un arbre contenant des entités
de différents types.

Un <def>répertoire</def> peut contenir des fichiers, répertoires et autres entités.
Les répertoires permettent de classer les choses.

<table>
<tr><td>        <td>Windows            <td>   Unix   </tr>
<tr><td>Français<td>Dossier            <td>Répertoire</tr>
<tr><td>Anglais <td><lang>Folder</lang><td><lang>Directory</lang> </tr>
</table>

Le <def>répertoire courant</def> (<lang>current directory</lang>)
est le répertoire dans lequel le <def>shell</def> se trouve actuellement.''')
    session.course('4:Commandes', '''
    Les commandes que vous allez découvrir vont être des plus simples
    et courammant utilisées aux plus complexes.''')
    session.course('4:Commandes/pwd', """
    La commande <cmd>pwd</cmd> <lang>Print Working Directory</lang>
    affiche le <def>chemin absolu</def> vers le
    <def>répertoire courant</def> du shell (donc l'endroit où vous êtes actuellement).""")
    session.ask('Lancez la commande <cmd>pwd</cmd>.')
    for _command in session.until(command_is='pwd'):
        session.tip(30, "Vous devez simplement taper <cmd>pwd</cmd> (sans les «»)")
    session.press_enter(screen_contains='$ pwd\n/')
    screen = session.get_screen()
    if screen:
        path = '/' + screen.split('$ pwd\n/')[1].split('\n')[0]
    else:
        path = '/home/p1234567'
    session.print(f"""
Le répertoire courant du shell est donc actuellement :

     <PATH>{path}</PATH>

C'est un <def>chemin absolu</def> car il commence par le caractère «<path>/</path>».
""")
