NEEDS = ['CMD_ls', 'KBD_completion_commande', 'SYNTAX_option_longue']
PROVIDES = ['KBD_completion_option']

INIT = """
cd /etc
"""

def run(session):
    '''Explique '''
    session.course('2:Chemins/Complétion',
        """On peut faire la complétion sur les options des commandes
        si le shell est complètement configuré. Il suffit de compléter après le «-».""")
    session.ask("Tapez la commande <cmd>ls --c</cmd> <important>sans lancer la commande</important>.")
    session.expect('CMD_ls')
    for _screen in session.until(screen_contains="\n--classify"):
        session.wait(command_is="ls --c")
        commande = session.get_next_command()
        session.transient("Recommencez : vous ne deviez pas lancer la commande",
                          commande == "")
        if commande == "ls --c":
            session.ask("Listez les complétions possibles.")

    session.ask("Choisissez la colorisation et lancez la commande.")
    session.wait(screen_contains="xdg")

    session.print("En blanc se sont les fichiers, en bleu les répertoires.")
