# Competence needed to answer this question
NEEDS = ['PROC_currentDir']
# Competence automaticaly given on good answer
PROVIDES = ['CMD_id', 'PROC_login']

def run(session):
    '''Exécutable'''
    session.course('4:Commandes/id',
        '''La commande <cmd>id</cmd> affiche votre identité (<def>login</def> et <def>UID</def>)
        et votre groupe actuel (<def>GID</def>).''')
    session.ask("Lancez la commande <cmd>id</cmd>")
    for _screen in session.until(screen_contains='id'):
        pass
    session.press_enter(screen_contains='uid=')
    session.course('1:Entités/Utilisateur',
        '''Le <def>login</def> est le nom que l'utilisateur utilise pour s'identifier.
        l'<def>UID</def> est le numéro unique associé à ce nom.

        Les utilisateurs sont dans un groupe par défaut,
        identifié par un <def>GID</def>.

        Toutes les entités du <def>système de fichier</def> ont un utilisateur
        propriétaire et un groupe propriétaire qui permettent de définir
        les droits d'accès.
        ''')
