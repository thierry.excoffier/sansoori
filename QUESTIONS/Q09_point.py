# Competence needed to answer this question
NEEDS = ['FS_hidden', 'SYNTAX_option_concat']
# Competence automaticaly given on good answer
PROVIDES = ['FS_point']

INIT = """
mkdir TRUC TOTO MACHIN BIDULE
cd $(
    for I in *
    do
    touch $I/un_fichier
    mkdir $I/UN_REPERTOIRE
    echo $RANDOM $I
    done |
        sort |
        (read A B; echo $B)
    )
"""

def ls_la(session):
    session.ask("""Listez le contenu du répertoire courant avec tous les détails,
en affichant aussi les fichiers cachés.""")
    session.expect('CMD_ls')
    for command in session.until(last_command_is=('ls -la', 'ls -al')):
        if session.transient("<CONCAT>", command.count('-') == 2):
            session.mul('SYNTAX_option_concat', 0.9)
        if session.get_command() == 'ls -l' and session.get_next_command() == '':
            session.tip(1, "Cela n'a pas affiché les fichiers cachés.")
        if session.get_command() == 'ls -a' and session.get_next_command() == '':
            session.tip(2, "Cela n'a pas affiché les détails sur les fichiers.")
        if session.tip(30,
                """C'est la commande <cmd>ls</cmd>
                avec les options <opt>a</opt> et <opt>l</opt>"""):
            session.mul('CMD_ls_a', 0.9)
    session.wait(screen_contains_all=(' .\n', ' ..', 'drwx'))

def run(session):
    '''.'''
    ls_la(session)
    session.ask("Affichez le chemin absolu du répertoire courant.")
    session.expect('CMD_pwd')

    session.ask("""
Allez dans le répertoire <path>.</path> qui est votre répertoire courant.
Cela ne va rien faire puisque vous demandez à aller à l'endroit ou vous êtes !""")
    session.expect('CMD_cd')
    session.wait(last_command_is='cd .')

    session.ask("""
Affichez à nouveau le chemin absolu du répertoire courant
pour vérifier qu'il n'a pas changé.""")
    session.wait(last_command_is='pwd')

    session.course('2:Chemins', """
Le répertoire <path>.</path> représente le répertoire le contenant.
Le chemin relatif <path>.</path> représente donc toujours le répertoire courant
quelque soit l'endroit où l'on se trouve.""")
    session.sleep(2)