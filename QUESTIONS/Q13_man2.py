NEEDS = ['CMD_man', 'CMD_less_next']
PROVIDES = ['CMD_man_navigate']

def run(session):
    '''Explique man'''
    session.ask("Affichez la page du manuel de la commande <cmd>gcc</cmd>.")
    session.expect('CMD_man')
    session.wait(command_is="man gcc")
    session.press_enter(screen_contains="GCC(1)")

    session.ask("Cherchez «pipeline» en utilisant la recherche de texte.")
    session.wait(screen_contains="/pipeline")
    session.wait(screen_contains="selective scheduling")
    session.ask("Cherchez <b>tous</b> les endroits suivants où se trouve «pipeline» sans retaper «/pipeline»")
    for screen in session.until(screen_contains='Pattern not found'):
        if '/pipeline' in screen:
            session.fail("Vous n'allez pas retaper «/pipeline» plein de fois !")

    session.print("Le message en bas de page vous indique qu'il n'y a plus rien à trouver")
    session.ask("Quittez le manuel.")
    session.wait(command_is="man gcc")
