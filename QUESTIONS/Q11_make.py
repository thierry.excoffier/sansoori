# Competence needed to answer this question
NEEDS = ['FS_navigate', 'CMD_ls', 'CMD_cat', 'SYNTAX_argument', 'SYNTAX_execute']
# Competence automaticaly given on good answer
PROVIDES = ['CMD_make']

INIT = """
echo '#include <iostream>
using namespace std;

int main()
{
    cout << "Bonjour !" << endl;
    return 0;
}' >hello.cpp
"""

def run(session):
    '''make'''
    session.ask('Listez les noms des entités contenues dans le répertoire courant')
    for _screen in session.until(screen_contains="hello.cpp"):
        pass
    session.print('<path>hello.cpp</path> est un code source en langage C++.')
    session.ask("Affichez le contenu du fichier pour vérifier.")
    session.expect('CMD_cat')
    for _screen in session.until(screen_contains="Bonjour"):
        pass
    session.course('4:Commandes/make',
        """La commande <cmd>make</cmd> (fabriquer) permet de transformer
        un fichier en un autre fichier.

        On lui indique en argument ce que l'on veut fabriquer et elle
        le fait à partir de ce qui est disponible à partir du répertoire courant.

        La commande <cmd>make</cmd> affiche alors toutes les commandes
        qui sont lancées pour faire la construction.
        Elle s'arrête à la première erreur.
        """)
    session.ask('Lancez <cmd>make</cmd> avec <arg>hello</arg> comme argument')
    for _screen in session.until(screen_contains="-o hello"):
        pass
    session.print('''Le fichier <path>hello</path> a été généré sans erreur,
    C'est un programme exécutable par le système d'exploitation.''')
    session.ask("Lancez l'exécution de <path>hello</path>")
    for _screen in session.until(screen_contains="Bonjour !\n$"):
        if session.tip(30, "Il faut lancer <cmd>./hello</cmd>"):
            session.mul("SYNTAX_execute", 0)
