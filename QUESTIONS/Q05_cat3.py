# Competence needed to answer this question
NEEDS = ['CMD_cat', 'FS_path']
# Competence automaticaly given on good answer
PROVIDES = ['FS_navigate']

INIT = """
for A in TOTO TITI TRUC MACHIN
do
  mkdir $A
  for B in x y z
  do
    echo "Je suis le contenu du fichier '$B' dans le répertoire '$A'" >$A/$B
  done
done
"""

def run(session):
    '''Plusieurs paramètres pour 'cat' '''
    dirname = session.choice('TOTO TITI TRUC MACHIN'.split(' '))
    filename = session.choice('xyz')
    session.ask(f'''
    Affichez le contenu du fichier <path>{filename}</path> se trouvant
    dans le répertoire <path>{dirname}</path> qui est dans votre répertoire courant.
    ''')
    session.expect('CMD_cat')
    for _screen in session.until(screen_contains=f"{filename}' dans le répertoire '{dirname}"):
        command = session.get_command()
        if session.transient(f"""
                Vous êtes en train d'écrire que <path>{dirname}</path> est
                contenu dans un répertoire nommé <path>{filename}</path>""",
                    command.startswith(f'cat {filename}/{dirname}')):
            session.mul('FS_path', 0.5)
        if session.transient(f"""
                <path>{filename}</path> n'est pas dans le répertoire courant
                mais dans le répertoire <path>{dirname}</path> qui lui
                est dans le répertoire courant.""",
                    command.startswith(f'cat {filename}')):
            session.mul('FS_path', 0.5)
