from Q06_elsewhere import INIT, get_choices # pylint: disable=unused-import

# Competence needed to answer this question
NEEDS = ['CMD_ls', 'FS_path', 'SYNTAX_argument']
# Competence automaticaly given on good answer
PROVIDES = ['FS_path_absolute']

def run(session):
    '''Explique le chemin absolu'''
    dirname, expect = session.choice(get_choices(session, absolute=True))
    session.course('2:Chemins', '''
Un <def>chemin absolu</def> est un chemin qui commence par le caractère <path>/</path>
il part donc de la racine et permet d'accéder à tous les fichiers.

La commande <cmd>ls</cmd> peut prendre un chemin comme <def>argument</def>
ce chemin peut être relatif ou absolu.
''')
    session.ask(f'''
Affichez le contenu du répertoire indiqué par le chemin absolu <path>{dirname}</path>
<b>sans changer votre répertoire courant</b>.
''')
    for screen in session.until(screen_contains_one=expect):
        if 'cd' in screen:
            session.cheat("Vous ne deviez pas utiliser la commande «cd»")
        session.transient("<SPACE>", f'$ ls{dirname}' in screen)
