# Competence needed to answer this question
NEEDS = ['KBD_completion_middle', 'CMD_cat']
PROVIDES = ['KBD_completion_list']

INIT = """
cd /etc
"""

CHOICES = (
    ('pa', 'size', 'papersize', 'a4'),
    ('pr', 'e'   , 'profile'  , '/etc/profile.d/*.sh'),
)

def run(session):
    '''Complétion list'''
    session.print("Vous devez répondre à cette question sans utiliser <cmd>ls</cmd>")
    start, end, expected, content = session.choice(CHOICES)

    session.course("2:Chemins/Complétion",
        """Quand la complétion n'affiche rien de plus, vous appuyez
        une deuxième fois sur la touche <key>Tabulation</key>
        pour lister toutes les suites possibles.""")

    session.ask(f"""Affichez le contenu du fichier du répertoire courant
    dont le nom commence par «{start}» et se termine par «{end}»""")
    session.expect('CMD_cat')
    for command in session.until(command_startswith=f'cat {expected}'):
        if command.startswith(f'cat {start}'):
            session.tip(5, f"Tapez «{start}»")
        if command.startswith(f'cat {start}'):
            session.tip(10, """Appuyez une (ou deux fois pour lister)
                sur la touche <key>Tabulation</key> pour compléter.""")
        if command.startswith('ls'):
            session.cheat("""Vous ne deviez pas utiliser <cmd>ls</cmd>
                mais la touche <key>Tabulation</key>""")
    session.press_enter(screen_contains=content)
