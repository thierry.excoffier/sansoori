# Competence needed to answer this question
NEEDS = ['KBD_completion_middle']
PROVIDES = ['KBD_completion_empty']

INIT = "cd /"


def run(session):
    '''Complétion list'''
    session.course('2:Chemins/Complétion',
    '''Il est possible de demander la complétion sans rien avoir commencé
    à écrire, dans ce cas tous les choix possibles sont affichés.''')
    session.ask("Tapez <cmd>cat</cmd> puis espace puis 2 fois la touche <key>Tabulation</key>.")
    for _screen in session.until(screen_contains='bin/'):
        pass
    session.print("""En procédant de cette manière cela permet de lister le
    contenu du répertoire courant sans avoir besoin d'interrompre la saisie
    de la commande en cours.""")

    session.ask("""Complétez maintenant <cmd>cat /var/</cmd>
        en appuyant 2 fois la touche <key>Tabulation</key>.""")
    for _screen in session.until(screen_contains='log/'):
        pass
    session.print("""En procédant de cette manière cela permet aussi de lister le
    contenu d'un autre répertoire sans avoir besoin d'interrompre la saisie
    de la commande en cours.""")
