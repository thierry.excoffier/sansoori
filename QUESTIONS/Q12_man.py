NEEDS = ['CMD_less_search']
PROVIDES = ['CMD_man']

def run(session):
    '''Explique man'''
    session.course('4:Commandes/man',
        """La commande <cmd>man</cmd> permet de lire les manuels utilisateurs
        des commandes actuellement installées.

        Il est préférable d'utiliser <cmd>man</cmd> plutôt que le web
        pour afficher la documentation car sur le web vous aurez
        des informations sur une commande qui n'est pas forcément la
        même que celle installée sur la machine.""")
    session.course('4:Commandes/man',
        """<cmd>man ls</cmd> vous affiche le manuel de la commande <cmd>ls</cmd>
        en utilisant la commande <cmd>less</cmd> pour l'afficher.""")
    session.ask("""Affichez le manuel de la commande <cmd>cat</cmd>""")
    session.expect('CMD_man')
    for _command in session.until(command_is="man cat"):
        session.tip(10, "Indiquez <cmd>cat</cmd> comme argument de la commande <cmd>man</cmd>.")
    session.press_enter(screen_contains="CAT(1)")

    session.ask("""Quittez le manuel, n'oubliez pas c'est la commande <cmd>less</cmd>
        qui fait l'affichage.""")
    for _command in session.until(command_is="man cat"):
        session.tip(10, "Lisez l'aide en bas de l'écran...")
