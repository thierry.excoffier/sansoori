# Competence needed to answer this question
NEEDS = ['FS_path']
# Competence automaticaly given on good answer
PROVIDES = ['CMD_cd', 'SYNTAX_argument']

def run(session):
    '''La commande «cd»'''
    dirname = session.choice(['/etc', '/bin', '/tmp', '/usr', '/var'])
    session.course('3:Syntaxe', '''
    Le premier mot de la ligne est la commande que l'on veut
    exécuter, les suivants sont des <def>arguments</def>.
    <important>Les mots sont séparés par des espaces comme dans toutes les langues.</important>''')
    session.sleep(3)
    session.course('4:Commandes/cd', '''
La commande <cmd>cd</cmd> (pour <lang>change directory</lang>)
change le <def>répertoire courant</def> du shell.

On indique un unique <def>argument</def> à la commande,
c'est le chemin vers le nouveau <def>répertoire courant</def> :
<ul>
   <li> <cmd>cd <path>/etc</path></cmd> elle utilise un <def>chemin absolu</def> ;
   <li> <cmd>cd <path>/usr/include</path></cmd> ici aussi ;
   <li> <cmd>cd <path>QUESTIONS</path></cmd> celle-ci utilise un <def>chemin relatif</def>
    car il ne commence pas à la racine, le premier caractère n'est pas <path>/</path>.
</ul>
''')
    session.ask(f'Lancez la commande <cmd>cd</cmd> pour aller dans <path>{dirname}</path>.')
    session.expect('CMD_cd')
    for command in session.until(command_startswith=f'cd {dirname}'):
        session.transient('<SPACE>', command.startswith('cd/'))
        session.tip(30, f"Vous devez simplement taper <cmd>cd <path>{dirname}</path></cmd>")
    session.press_enter(screen_contains=f'$ cd {dirname}\n$')
    session.ask(f"""
        Vérifiez maintenant que votre <def>répertoire courant</def>
        est <path>{dirname}</path>""")
    session.expect('CMD_pwd')
