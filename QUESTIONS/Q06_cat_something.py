# Competence needed to answer this question
NEEDS = ['CMD_cat', 'CMD_ls']
# Competence automaticaly given on good answer
PROVIDES = ['FS_navigate']

INIT = """
echo "Préparer mes cours pour demain" >todo
"""

def run(session):
    '''Exo cat'''
    session.ask("Affichez le contenu de l'unique fichier contenu dans le répertoire courant.")
    session.expect('CMD_ls')
    session.press_enter(screen_contains='todo')
    session.expect('CMD_cat')
    session.press_enter(screen_contains='demain')
    session.print("Vous connaissez maintenant la chose la plus importante pour réussir.")
