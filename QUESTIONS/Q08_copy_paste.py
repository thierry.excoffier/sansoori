NEEDS = ['FS_navigate', 'KBD_completion_full']
PROVIDES = ['copypaste']

CMD = "cat /proc/self/net/stat/arp_cache /proc/self/net/stat/ndisc_cache"
INIT = f"""
echo '

#####################################
# Affiche le type de mon processeur #
#####################################
cat /proc/cpuinfo

###################################
# Affiche les statistiques réseau #
###################################
{CMD}

' > mes_commandes_magiques_rien_que_pour_moi

touch truc bidule machin chose
mkdir TP_INF1010L TP_INF1012L

"""


def run(session):
    """Explique copier/coller"""
    session.ask("""Affichez le contenu du fichier nommé :
        <path>mes_commandes_magiques_rien_que_pour_moi</path>
        se trouvant dans le répertoire courant, en utilisant la complétion.""")
    session.expect('CMD_cat')
    session.wait(screen_contains='/proc/cpuinfo')
    session.course("0:Introduction",
        """Unix permet le copier/coller habituel avec <key>Ctrl+C</key> et <key>Ctrl+V</key>.

        Mais il permet aussi de faire des copier/coller rapide :

        <ul>
        <li> Vous sélectionnez le texte avec le bouton de gauche de la souris.
        <li> Vous collez en cliquant avec le bouton du milieu (la molette).
        </ul>
        """)
    session.course("0:Introduction",
        """Dans le shell le texte collé s'ajoute à l'endroit où se trouve
        le curseur texte (le carré blanc) et non à l'endroit où se trouve
        la souris.

        Ceci permet de coller rapidement sans avoir besoin de viser
        l'endroit ou l'on veut insérer, il suffit d'être dans le terminal.
        """)

    session.ask("""Sélectionnez en utilisant le bouton de gauche,
        la commande pour afficher les statistiques réseaux.""")
    session.wait(selection_contains=CMD)
    session.ask("""Collez en cliquant n'importe où dans la fenêtre
        avec le bouton du milieu (la molette).""")
    session.wait(command_is=CMD)
    session.press_enter(screen_contains='lookups')
