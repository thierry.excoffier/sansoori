NEEDS = ['CMD_less_search']
PROVIDES = ['CMD_less_next']
INIT = """
cd /etc/dictionaries-common
"""

def run(session):
    '''Explique n'''
    session.ask('Affichez le contenu du fichier <path>words</path> <b>page par page</b>.')
    session.expect('CMD_less')
    session.wait(screen_startswith="A\n")

    session.ask("Affichez maintenant la page d'aide de <cmd>less</cmd>")
    for _screen in session.until(screen_contains='^E'):
        if session.tip(10, "C'est la touche <key>h</key> comme <lang>help</lang>"):
            session.mul('CMD_less', 0.9)

    session.ask("Cherchez dans l'aide le texte «Repeat».")
    for _screen in session.until(screen_contains='previous search'):
        if session.tip(10, 'Vous devez simplement taper «/Repeat».'):
            session.mul('CMD_less_search', 0.9)

    session.print("""Cela vous explique comment chercher l'emplacement suivant
        contenant le texte que vous aviez cherché.""")
    session.course("4:Commandes/less",
        """<key>n</key> comme <lang>next</lang> permet de répéter
        la dernière recherche qui a été faite.""")

    session.ask("Quittez l'aide.")
    session.wait(screen_startswith="A\n")

    session.ask("Cherchez le texte «earl».")
    session.wait(screen_contains="Pearl")

    session.ask("""Affichez les occurrences suivantes,
        en répétant la dernière recherche,
        allez-y doucement et observez ce qu'il se passe.""")
    session.wait(screen_contains="early")

    session.ask("Quittez le programme <cmd>less</cmd>.")
    session.wait(screen_contains="$ less words")
