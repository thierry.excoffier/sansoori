# Competence needed to answer this question
NEEDS = ['SYNTAX_option_courte', 'PROC_currentDir']
# Competence automaticaly given on good answer
PROVIDES = ['FS_hidden', 'CMD_ls_a']

INIT = """
mkdir COURS LOISIRS
echo "Une coquille vide." >readme
echo "Petit curieux... 😃" >.mon_secret
"""

def run(session):
    '''Options de commande'''

    session.ask("Affichez les entités contenues dans le répertoire courant")
    session.expect('CMD_ls')
    session.press_enter(screen_contains='COURS')

    session.ask("""
Affichez les entités contenues dans le répertoire courant
en indiquant cette fois l'option «<opt>a</opt>».
""")
    for command in session.until(command_is='ls -a'):
        if session.transient("Après le nom de la commande il faut un espace.",
                             command.startswith(('lsa', 'ls-'))):
            session.mul('SYNTAX_argument', 0.9)
        if session.transient("Vous avez oublié le tiret devant le «<opt>a</opt>»",
                             command.startswith('ls a')):
            session.mul('SYNTAX_option_courte', 0.9)
        if session.tip(10, "Indiquez maintenant l'option «<opt>a</opt>»"):
            session.mul('SYNTAX_option_courte', 0.9)

    session.press_enter(screen_contains='..')

    session.course('4:Commandes/ls', '''
L'option «<opt>a</opt>» fait afficher les fichiers cachés.
Ce sont les fichiers dont le nom commence par «.».
Cela va donc toujours afficher <path>.</path> et
<path>..</path> qui sont présents dans <b>tous</b> les répertoires.''')
    session.print("<path>.mon_secret</path> est un fichier créé par l'utilisateur.")
