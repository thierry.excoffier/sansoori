# Competence needed to answer this question
NEEDS = ['KBD_completion_middle', 'CMD_ls', 'FS_path_absolute']
PROVIDES = ['KBD_completion_dir']

CHOICES = (
    ('/usr/include', 'c', ('/usr/inc', '/usr/incl', '/usr/inclu', '/usr/includ'), '.h'),
    ('/etc/python', 't', ('/etc/pyt', '/etc/pyth', '/etc/pytho', 'filename'), 'debian_config')
)

def run(session):
    '''Complétion list'''
    dirname, letter, forbid, expect = session.choice(CHOICES)
    session.course('2:Chemins/Complétion',
        'La complétion complète les chemins contenant des «/»')
    session.ask(f"""Listez le contenu du répertoire <path>{dirname}</path>
    sans utiliser la touche <key>{letter}</key> du clavier, donc avec la complétion.""")
    session.expect('CMD_ls')
    for command in session.until(command_startswith=f'ls {dirname}'):
        if command.endswith(forbid):
            session.cheat(f"""Vous ne deviez pas utiliser <key>{letter}</cmd>
                mais la touche <key>Tabulation</key>""")
    session.press_enter(screen_contains=expect)
