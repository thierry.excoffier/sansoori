NEEDS = ['FS_path_absolute', 'CMD_cat']
PROVIDES = ['KBD_scroll']

def run(session):
    '''Scroll with keyboard'''
    dirname, filename = ('/proc', 'cpuinfo')
    session.ask(f'''Affichez le contenu complet du fichier <path>{filename}</path>
        se trouvant dans le répertoire <path>{dirname}</path>.''')
    session.expect('CMD_cat')
    session.wait(command_contains=f'cat {dirname}/{filename}')
    session.press_enter(next_command_is='')

    session.course('2:Chemins/Interaction',
        """<key>Majuscule</key>+<key>Page Précédente</key> permet de
        voir ce qui a disparu vers le haut quand trop de choses ont
        été affichées.

        Le symbole sur la touche page précédente peut être <key style="font-family:emoji">⇞</key>
        et la touche majuscule <key style="font-family:emoji">⇧</key>.""")

    session.ask("Faites afficher le début du fichier.")
    session.wait(screen_contains='processor       : 0')
    session.course('2:Chemins/Interaction',
        """Quand on tape un caractère, l'écran remonte pour faire afficher
        la ligne de commande que vous êtes en train de taper.""")
    session.ask("Tapez «a»")
    session.wait(command_is='a')
