# Competence needed to answer this question
NEEDS = ['PROC_currentDir']
# Competence automaticaly given on good answer
PROVIDES = ['FS_path']

INIT = "cd /usr/include"

def run(session):
    '''Retrouver la commande «pwd»'''
    session.ask('Faites afficher le chemin vers le <def>répertoire courant</def>.')
    session.expect('CMD_pwd')
    text = []
    screen = session.get_screen()
    if screen:
        path = screen.strip().split('\n')[-2]
    else:
        path = '/usr/include/c++'
    text.append(f"Le chemin <path>{path}</path> est absolu, il indique que :<ul>")
    path = path.split('/')[::-1]
    text.append(f'<li> vous êtes dans <path>{path[0]}</path>')
    for name in path[1:-1]:
        text.append(f'<li> qui est dans <path>{name}</path>')
    text.append('''
    <li>qui est à la racine (rien au dessus).
    </ul>
    Le premier <path>/</path> indique la racine et les suivants veulent dire 'dedans'.

    Comme pour adresse géographique :<br>
    <path>/FR/69/Villeurbanne/Av_Coub…/23/Nautibus/1er/TP10</path>
    ''')
    session.course('2:Chemins', ''.join(text))
