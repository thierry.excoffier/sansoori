# Competence needed to answer this question
NEEDS = ['SYNTAX_argument']
# Competence automaticaly given on good answer
PROVIDES = ['CMD_cat']

INIT = """
echo "Que mange-t-on ce soir ?" >question
"""

def run(session):
    '''Explique fichier et cat'''
    session.course('1:Entités', '''
Un <def>fichier texte</def> contient une suite d'octets.
Cette suite d'octets peut représenter du texte, une image, un son, une vidéo…
''')
    session.course('4:Commandes/cat', '''
    La commande <cmd>cat</cmd> affiche sur l'écran le contenu du fichier indiqué.
    Elle n'interpète pas le contenu du fichier,
    ceci veut dire que si le fichier contient une image,
    elle va afficher l'image comme une suite de caractères incompréhensibles.''')
    session.ask('Lancez la commande <cmd>cat</cmd> avec comme argument <path>question</path>')
    session.expect('CMD_cat')
    for _command in session.until(command_startswith='cat question'):
        session.tip(30, "Le premier argument doit être <path>question</path>")
    session.press_enter(screen_contains='Que')
    session.print("Vous voyez maintenant le contenu du fichier nommé <path>question</path>.")
