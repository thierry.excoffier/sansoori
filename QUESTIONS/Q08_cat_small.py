NEEDS = ['CMD_ls_l', 'CMD_cat']
PROVIDES = ['FS_navigate']

INIT = """

for I in MACHIN TRUC BIDULE
do
    mkdir $I
    echo "/* $I/README */
Lancez «make» pour compiler et exécuter le programme" >$I/README
    echo "# /* $I/Makefile */
CFLAGS=-Wall
run:main
\t./main
">$I/Makefile
    echo "/* $I/main.c */
#include <stdio.h>
int main() {
    printf(\\"Hello $I!\\n\\");
    return 0;
}" >$I/main.c
done

"""

def run(session):
    """Exo"""
    dirname = session.choice(('MACHIN', 'TRUC', 'BIDULE'))
    taille, filename = session.choice((
        ('le plus petit', 'Makefile'),
        ('le plus gros', 'main.c'),
        ('de taille moyenne', 'README'),
        ))

    session.ask(f"""Affichez le contenu du fichier {taille} se trouvant
        dans le répertoire <path>{dirname}</path>.""")
    for screen in session.until(screen_contains=f'/* {dirname}/{filename} */'):
        # TIP
        pass
