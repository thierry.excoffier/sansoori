NEEDS = ['SYNTAX_option_concat', 'CMD_ls_a', 'CMD_man_navigate', 'CMD_less', 'FS_hidden']
PROVIDES = ['SYNTAX_option_longue']

INIT = """
echo 'Mes TP rangés par cours' >README
mkdir INF1010L INF1012L
"""

def run(session):
    '''Explique options longues'''
    session.ask("""Affichez toutes les entités contenues dans le répertoire courant
        y compris les cachées.""")
    session.expect('CMD_ls')
    session.wait(screen_contains="..")
    session.print("Vous avez utilisé l'option courte <opt>a</opt> pour faire cet affichage.")
    session.ask("Affichez la page de manuel de la commande <cmd>ls</cmd> et regardez la première option proposée.""")
    session.expect('CMD_man')
    session.wait(screen_contains="LS(1)")
    session.course("3:Syntaxe",
        """Une <def>option</def> longue est une option préfixée par «--»
        qui peut contenir plusieurs caractères.
        Par exemple <opt>--help</opt> est une option longue.
        """)
    session.print("""À droite de l'option <opt>a</opt> vous voyez la version longue
        de l'écriture de l'option.""")
    session.ask("Quittez le manuel.")
    session.wait(command_is="man ls")
    session.ask("Lancez la commande en utilisant l'option longue correspondant à <opt>-a</opt>.")
    session.expect('CMD_ls')
    session.wait(command_is="ls --all")
    session.course("3:Syntaxe",
        """On utilise rarement des <def>options longues</def> quand on tape des commandes au clavier,
        mais elles souvent utilisées dans les fichiers de commande (script)
        car c'est plus facile à relire.
        """)
    session.wait(next_command_is="")
    session.course("3:Syntaxe",
        """Les <def>options longues</def> ne peuvent pas être concaténées
        les unes derrière les autres.""")
