# Competence needed to answer this question
NEEDS = ['CMD_ls_l', 'FS_mode', 'FS_point']
# Competence automaticaly given on good answer
PROVIDES = ['SYNTAX_execute']

INIT = '''
cat >truc <<'%'
echo "Vous venez d'exécuter le programme nommé «truc»"
%
chmod 700 truc
'''


def run(session):
    '''Exécutable'''
    session.ask('Affichez les détails sur les entités du répertoire courant.')
    for _screen in session.until(screen_contains='-rwx------'):
        pass
    session.print('''Le fichier <path>truc</path> est donc exécutable par vous,
        vous en êtes propriétaire et vous pouvez le modifier.''')
    session.course('4:Commandes', '''
    Pour exécuter une commande se trouvant dans le répertoire courant,
    taper son nom n'est pas suffisant car c'est une faille de sécurité.

    Pour lancer le programme, vous devez indiquer explicitement
    qu'il est dans le répertoire courant en indiquant qu'il est dans
    le répertoire <path>.</path> : <path>./programme</path>
    ''')
    session.ask('Exécutez <cmd>truc</cmd> qui se trouve dans le répertoire courant.')
    for screen in session.until(screen_contains='nommé «truc»'):
        if '$ .' not in screen:
            session.tip(30,
                "C'est dans <path>.</path> donc le chemin vers la commande commence par «.»")
        if '$ ./' not in screen:
            session.tip(40, "C'est dans <path>.</path> donc après le «.» il faut un «/»")
