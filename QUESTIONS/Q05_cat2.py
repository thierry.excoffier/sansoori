# Competence needed to answer this question
NEEDS = ['CMD_cat']
# Competence automaticaly given on good answer
PROVIDES = ['CMD_ls']

INIT = """
echo "Que mange-t-on ce soir ?" >q
echo "Des pâtes !" >r
"""

def run(session):
    '''Plusieurs paramètres pour 'cat' '''
    session.course('4:Commandes/cat', '''
    La commande <cmd>cat</cmd> peut prendre plusieurs chemins en paramètre,
    chacun des chemins est un argument de la commande.
    Le contenu de tous les fichiers indiqués est affiché sur l'écran.''')
    session.ask('Listez le contenu du répertoire courant.')
    session.expect('CMD_ls')
    session.press_enter(screen_contains='q')

    session.ask('''
    Faites afficher le contenu des 2 fichiers contenus dans le répertoire courant.

    <important>En lançant une seule fois la commande <cmd>cat</cmd>.</important>
    ''')
    session.expect('CMD_cat')
    for _screen in session.until(screen_contains_one=(
        'Que mange-t-on ce soir ?\nDes pâtes !',
        'Des pâtes !\nQue mange-t-on ce soir ?')):
        pass
