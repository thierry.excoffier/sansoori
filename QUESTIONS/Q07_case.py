import itertools

NEEDS = ['FS_path', 'CMD_ls', 'CMD_cat', 'FS_path_relative', 'FS_path_absolute']
PROVIDES = ['FS_path_case']
NAMES = tuple(''.join(i) for i in itertools.product('Tt', 'Rr', 'Uu', 'Cc'))
INIT = f"""
for A in {' '.join(NAMES)}
do
echo Perdu >$A
done
chmod 666 [tT]*
"""

def run(session):
    '''Complétion list'''
    session.course('2:Chemins', """
    Dans le nom des entités les minuscules et majuscules
    représentent des choses différentes.
    Elles ne sont pas interchangeables, il faut les respecter.

    <important>Le système d'exploitation Apple ne respecte pas cette règle,
    vous ne pouvez pas créer 2 fichiers dans le même répertoire dont
    la seule différence est la casse.</important>
    """)
    session.ask("Listez le contenu du répertoire courant")
    session.expect('CMD_ls')
    name = session.choice(NAMES)
    with open(session.home/name, "w", encoding="utf-8") as file:
        file.write("Gagné !\n")
    session.ask(f"Affichez le contenu complet (pas page par page) du fichier <path>{name}</path>")
    session.expect('CMD_cat')
    for _command in session.until(command_startswith='cat ' + name):
        pass
    session.press_enter(screen_contains='Gagné !')
