NEEDS = ['FS_path_relative', 'FS_path_absolute', 'CMD_cat']
PROVIDES = ['FS_navigate']

CHOICES = (
    ('/etc/default', 'ssh', 'SSHD_OPTS'),
    ('/etc/default', 'keyboard', 'XKBOPTIONS'),
    ('/etc/default', 'locale', 'LC_TIME'),
    ('/etc/python', 'debian_config', 'byte-compile'),
    ('/etc/apt', 'sources.list', 'http:'),
)

def run(session):
    """Exo ls + cat"""
    _dirname, filename, content = session.choice(CHOICES)
    dirnames = set(i[0] for i in CHOICES)
    session.ask(f"""Affichez le contenu du fichier <path>{filename}</path> se trouvant
        dans l'un de ces répertoires :
        <ul>{' '.join('<li><path>'+i+'</path>' for i in dirnames)}</ul>""")
    session.print("Vous pouvez répondre en lançant seulement 2 ou 3 commandes.")
    for _screen in session.until(screen_contains=content):
        session.tip(60,
            f"""Voici comment y arriver facilement :
                <ul>
                    <li> Allez dans <path>/etc</path> (changez de répertoire courant).
                    <li> Listez le contenu des répertoires indiqués sans
                        mettre le <path>/etc</path> car vous êtes dedans.
                    <li> Affichez le contenu du fichier <path>{filename}</path>
                </ul>""")
                    