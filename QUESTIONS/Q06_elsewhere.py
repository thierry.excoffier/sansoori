import os
# Competence needed to answer this question
NEEDS = ['CMD_ls', 'CMD_cd', 'SYNTAX_argument', 'FS_path']
# Competence automaticaly given on good answer
PROVIDES = ['FS_navigate']

INIT = """
mkdir TOTO TITI
touch TOTO/mon_fichier.txt TITI/ne_pas_lire
mkdir TOTO/MON_REPERTOIRE
echo "Répertoire de démo" >README
"""

def get_choices(session, absolute=False, relative=False):
    choices = []
    if absolute:
        choices.extend((
            ('/etc', tuple(os.listdir('/etc'))),
            (str(session.home), ('README',)),
            ))
    if relative:
        choices.extend((
            ('TOTO', ('mon_fichier.txt',)),
            ('TITI', ('ne_pas_lire',)),
            ))
    return choices

def run(session):
    '''Navigate in the tree'''
    dirname, expect = session.choice(get_choices(session, absolute=True, relative=True))
    session.ask(f'Affichez le contenu du répertoire :<br><path>{dirname}</path>.')
    for screen in session.until(screen_contains_one=expect):
        if session.transient("<SPACE>", f'$ cd{dirname[0]}' in screen):
            session.mul('SYNTAX_argument', 0.9)
        if session.transient("<SPACE>", f'$ ls{dirname[0]}' in screen):
            session.mul('SYNTAX_argument', 0.9)
        session.tip(10, """Par exemple vous pouvez changer de répertoire courant
puis lister ensuite son contenu""")
