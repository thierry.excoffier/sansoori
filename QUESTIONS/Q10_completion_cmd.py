# Competence needed to answer this question
NEEDS = ['KBD_completion_list']
PROVIDES = ['KBD_completion_commande']

INIT = "cd /"


def run(session):
    '''Complétion list'''
    session.course('2:Chemins/Complétion',
    '''On peut aussi demander la complétion sur le nom de la commande afin
    de voir toutes les commandes commençant par ce que vous avez tapé.''')
    session.ask("Affichez les commandes commençant par la lettre <key>z</key>")
    for _screen in session.until(screen_contains='zcat'):
        pass
    session.print("""C'est pratique quand on ne se souvient plus exactement
    du nom de la commande.""")
