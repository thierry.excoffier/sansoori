"""
Management of competences
"""

import os
import sys
import traceback
import ast
import shlex
import time
import importlib
import pathlib
import collections

class Competence : # pylint: disable=too-few-public-methods
    """A competence"""
    def __init__(self):
        self.value = 0

class Question: # pylint: disable=too-few-public-methods,too-many-instance-attributes
    """A question extracted from module"""
    depth = 0
    def __init__(self, module):
        self.module = module
        self.name = module.__name__
        self.filename = self.name.replace('.', '/', 1) + '.py'
        self.short_name = self.name.split('.')[1]
        self.needs = module.NEEDS
        self.provides = module.PROVIDES
        self.words = getattr(module, 'WORDS', {})
        self.run = module.run
        self.doc = self.run.__doc__
        self.init = module.__dict__.get('INIT', '')

class Questions:
    """The questions"""
    forced = None
    questions_dict = {}
    providers = collections.defaultdict(list) # competence name → questions objects
    words = {}

    def __init__(self, logname, home, load_log=True):
        self.giveup_on = None # The student giveup
        if not self.questions_dict:
            self.update_questionnary()
        self.competences = {}
        self.success = collections.defaultdict(int)
        self.fail = collections.defaultdict(int)
        for question in self.questions:
            for competence in question.needs:
                self.competences[competence] = Competence()
            for competence in question.provides:
                self.competences[competence] = Competence()

        if load_log and os.path.exists(logname):
            with open(logname, 'r', encoding='utf-8') as file:
                for line in file:
                    self.evaluate(ast.literal_eval(line))
        self.home = pathlib.Path(home)
        self.logname = pathlib.Path(logname)
        self.updated_competences = set()

    @classmethod
    def update_questionnary(cls):
        """Read or reread the questions"""
        dirname = f'{os.getcwd()}/QUESTIONS'
        if dirname not in sys.path:
            sys.path.append(dirname)
        cls.questions = []
        error = 'Reload success'
        for file in os.listdir('QUESTIONS'):
            if not file.endswith('.py'):
                continue
            name = file[:-3]
            if name in cls.questions_dict:
                try:
                    importlib.reload(cls.questions_dict[name].module)
                except: # pylint: disable=bare-except
                    error = traceback.format_exc()
            question = Question(importlib.import_module(f'QUESTIONS.{name}'))
            cls.questions.append(question)
            cls.questions_dict[question.short_name] = question
            cls.words.update(question.words)
            for competence in question.provides:
                cls.providers[competence].append(question)

        def question_depth(question):
            """Compute minimal distance from start"""
            if not question.depth:
                question.depth = 1 + max((question_depth(quest)
                                          for competence in question.needs
                                          for quest in cls.providers[competence]
                                         ), default=0)
            return question.depth
        for question in cls.questions:
            question_depth(question)
        return error

    ##########################################################################
    # log management
    ##########################################################################

    def evaluate(self, line):
        """Evaluate a log line"""
        if line[1] == 'add':
            self.competences[line[2]].value += line[3]
        elif line[1] == 'mul':
            self.competences[line[2]].value *= line[3]
        elif line[1] == 'success':
            self.success[line[2]] += 1
        elif line[1] in ('giveup', 'cheat', 'fail', 'close'):
            self.fail[line[2]] += 1

    def log(self, *data):
        """Store data in user log"""
        data = (int(time.time()),) + data
        with open(self.logname, 'a', encoding='utf-8') as file:
            file.write(repr(data) + '\n')
        self.evaluate(data)

    def add(self, competence, value):
        """Add the value to the competence"""
        self.log('add', competence, value)
        self.updated_competences.add(competence)
    def mul(self, competence, value):
        """Add the value to the competence"""
        self.log('mul', competence, value)
        self.updated_competences.add(competence)
    def log_success(self, question):
        """Student success"""
        self.log('success', question.name)
        for competence in question.provides:
            self.add(competence, 1)
    def log_close(self, question):
        """Browser close"""
        self.log('close', question.name)
    def log_cheat(self, question):
        """Student cheated"""
        self.log('cheat', question.name)
    def log_fail(self, question):
        """Student fail to answer"""
        self.log('fail', question.name)
    def log_giveup(self, question):
        """The student give up"""
        self.log('giveup', question.name)
    def giveup(self, question):
        """Record the failure"""
        self.giveup_on = question
        for competence in question.needs:
            self.mul(competence, 0.9)
        for competence in question.provides:
            self.mul(competence, 0.5)

    def question_possible(self, question):
        """Returns True if the question is possible"""
        if not question.needs and not question.provides:
            return False
        for competence in question.needs:
            if self.competences[competence].value < 1:
                return False
        return True
    def choose_question(self):
        """Returns the best questions"""
        if self.forced:
            question = self.questions_dict[self.forced]
            self.forced = None
            return question
        questions = [
            question
            for question in self.questions
            if self.question_possible(question) and question is not self.giveup_on
            ]
        if not questions:
            return self.giveup_on # Can not giveup on first question
        self.giveup_on = None
        return min(questions,
            key=lambda question: min(self.competences[competence].value
                                     for competence in question.provides)
                                 + self.success[question.name]/1000000.
                                 + question.depth/1000.)
    def __getitem__(self, name):
        """Get a question from name or path"""
        for question in self.questions:
            if question.filename == name:
                return question
        return None

    def nr_competences(self):
        """Number of acquired competences"""
        return len([c for c in self.competences.values() if c.value >= 1])

    def stats(self):
        """Statistics"""
        print(f"{len(self.questions)} questions et {len(self.competences)} compétences :")
        for competence_name, competence in sorted(self.competences.items(),
                                                  key = lambda x: -x[1].value):
            print(f"{competence_name:>15} {competence.value:3.1f}")

    def short_stats(self):
        """Short"""
        return ' '.join(f'{competence}={c.value:3.1f}'
                        for competence, c in self.competences.items())

    def bash_command(self, login, question):
        """Launch bash"""
        profile = """
export PS1='$ '
export PROMPT_COMMAND=''
"""
        if callable(question.init):
            profile += question.init(self)
        else:
            profile += question.init
        return f"exec ./launcher {login} {shlex.quote(profile)}"
