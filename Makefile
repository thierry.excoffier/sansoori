
run:install kill # For DEV launch in foreground
	./killer &
	./sansoori.py

restart:install kill # For PROD launch in background
	-pkill -u sansoori -9 sansoori.py
	-pkill killer
	./killer >logs.killer 2>&1 </dev/null &
	./sansoori.py >logs.sansoori 2>&1 </dev/null &
	sleep 1

kill:
	@echo "Kill running Sansoori"
	@-pkill --oldest --full -1 killer 2>/dev/null
	@-pkill --oldest --full -1 sansoori.py 2>/dev/null


######################## DEV CONFIGURATION ###########################

install:LOGS STATS config.py killer launcher node_modules/xterm favicon.ico xxx-course.html xxx-graph.svg

LOGS STATS:
	mkdir $@

config.py:
	@echo '# 'config.py' file is Python/Makefile/Shell compatible' >$@
	@echo '# You must edit it!' >>$@
	@echo '' >>$@
	@echo '# Sansoori administrator list.' >>$@
	@echo 'SANSOORI_ADMINS="$(shell id -un)"' >>$@
	@echo '# Only UID allowed to run ./launcher' >>$@
	@echo 'SANSOORI_UID="$(shell id -u)"' >>$@
	@echo '# Home directory for the questions' >>$@
	@echo 'SANSOORI_HOME="/Users"' >>$@
	@echo '# For socket listening' >>$@
	@echo 'SANSOORI_IP="127.0.0.1"' >>$@
	@echo '# The server HTTP port' >>$@
	@echo 'SANSOORI_PORT=9999' >>$@
	@echo '# Authenticate with CAS :' >>$@
	@echo 'SANSOORI_CAS="https://cas.univ-lyon1.fr"' >>$@
	@echo '# or not...' >>$@
	@echo 'SANSOORI_CAS=None' >>$@
	@echo 'SANSOORI_PROD_HOST="sansoori.univ-lyon1.fr"' >>$@
	@echo 'SANSOORI_PROD_USER="sansoori"' >>$@
	@echo 'SANSOORI_PROD_ROOT="root"' >>$@
	@echo 'SANSOORI_AUTH_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDBvv4ir/EEGb4PKr8a9257vp1DcbmKOuqE2RpXp9JMnmM9HR+nKJN67/hr1AHigFUV8+5pLRbrZNi6goI5HLSZJz3zWuWWwurtufx0andqLbk3WXpxF0hmT8ylvtSGo1mjXnWpNscuJ0YnHOU2xca5ZANGM0UDlU1uKOGlJxv0WAL0tbnpKrVjAS97cpA1z7KjFceZhfzkQRfiYCk4T6CZNqRrQdYsjAFNIeShTiKYG9W1lA7W0+MATF1u4cm1bBnoQxLur+1/vbzm5rm2fvI027fbN2VIOjgsJ7zG+Cm7I6Q4HZ9Yk5GuZCaAyOB8lj6ajpUhpF+npS8gI11b7e9 exco@pundit"' >>$@
	@echo 'SANSOORI_CERT_BOT="your@mail.address"' >>$@
	@cat $@
	@echo "EDIT IT NOW"
	exit 1

include config.py

killer:killer.c

node_modules/xterm:
	npm install xterm

SANSOORI_FIRST_ADMIN=$(shell echo $(SANSOORI_ADMINS) | (read A B; echo $$A))

LAUNCHER = chown root launcher ; chmod 4711 launcher

launcher:launcher.c
	$(CC) -Wall -DUSER='"$(SANSOORI_FIRST_ADMIN)"' \
	            -DSANSOORI_UID=$(SANSOORI_UID) \
			    -DHOME='$(SANSOORI_HOME)' \
				$@.c -o $@
	if sudo -n true 2>/dev/null; then sudo sh -c "$(LAUNCHER)" ; else su -c "$(LAUNCHER)" ; fi || true
	# Check if the launcher is working (should return 0 code)
	echo | ./launcher $(SANSOORI_FIRST_ADMIN) '' 2>/dev/null >&2 || true

favicon.ico:icon.svg
	inkscape --export-height 64 --export-width 64 --export-area-drawing --export-filename=xxx.png $?
	mv xxx.png $@

######################## PROD CONFIGURATION ###########################

xxx.prod_configured:config.py
	# The local config.py is only used for startup.
	# The final one will be created on SANSOORI_PROD_USER account
	(cat config.py configure_prod) | ssh $(SANSOORI_PROD_ROOT)@$(SANSOORI_PROD_HOST) 2>&1 | tee $@

install-prod:xxx.prod_configured
	git diff --exit-code # Do the commit before
	git archive --prefix ./ HEAD | ssh $(SANSOORI_PROD_USER)@$(SANSOORI_PROD_HOST) "tar -xvf - ; chmod 700 ."
	ssh $(SANSOORI_PROD_ROOT)@$(SANSOORI_PROD_HOST) "cd /home/$(SANSOORI_PROD_USER) ; make launcher"
	# Check if the launcher is working (should return 0 code)
	echo | ./launcher $(SANSOORI_FIRST_ADMIN) '' 2>/dev/null >&2 || true
	scp xxx-*-*.png xxx-course.html xxx-graph.svg $(SANSOORI_PROD_USER)@$(SANSOORI_PROD_HOST):

restart-prod:install-prod
	ssh $(SANSOORI_PROD_USER)@$(SANSOORI_PROD_HOST) "make restart"

######################## UPSTREAM ONLY USAGE ###########################

xxx-graph.svg:QUESTIONS/* Makefile
	./sansoori.py graph $(SANSOORI_FIRST_ADMIN) >xxx-graph.svg

xxx-1.png:kill QUESTIONS/* Makefile
	@echo "Create screenshots (very very long)"
	@mv config.py config.py.orig ; \
	./sansoori.py 2>/dev/null >&2 & \
	sleep 0.1 ; \
	cp config.py.orig config.py ; \
	./screenshot
	$(MAKE) kill

xxx-course.html:QUESTIONS/* Makefile
	./sansoori.py compute_course >xxx-course.html

documentation:xxx-graph.svg xxx-1.png xxx-course.html
	@echo "$$(ls QUESTIONS/Q* | wc -l) questions définies"

WWW=$(HOME)/public_html/SANSOORI
install-doc:documentation
	mkdir $(WWW) || true
	cp --update xxx-graph.svg xxx-course.html xxx-?.png $(WWW)


