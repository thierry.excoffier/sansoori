#!/usr/bin/python3
"""
npm install xterm
"""

# pylint: disable=consider-using-with

import asyncio
import json
import random
import pty
import os
import sys
import threading
import time
import re
import traceback
import urllib.request
import urllib.error
import urllib.parse
import pwd
import html
import aiohttp
from aiohttp import web
import core
import config

URL = f'https://{config.SANSOORI_PROD_HOST}/' # Only used for CAS authentication
CAS = config.SANSOORI_CAS
ADMINS = re.split(' +', config.SANSOORI_ADMINS)

class Cheat(Exception):
    """The student cheated to answer"""
class Fail(Exception):
    """The answer was bad"""
class StopQuestion(Exception):
    """Question stopped"""
class ConnectionClose(Exception):
    """Broken connection"""

def get_file(request):
    """Get mime type and content"""
    path = request.path
    if path in ('/', '/erase'):
        path = "/sansoori.html"
    if path.endswith('.css'):
        mime = 'text/css'
    elif path.endswith('.js'):
        mime = 'application/javascript'
    elif path.endswith('.ico'):
        mime = 'image/png'
    elif path.endswith('.svg'):
        mime = 'image/svg+xml'
    else:
        mime = 'text/html'
    with open(path[1:], 'rb') as file:
        content = file.read()
    return mime, content

async def load(request):
    """Load file"""
    mime, content = get_file(request)
    return web.Response(
        body=content,
        content_type=mime,
        charset='utf-8',
    )

async def run(request):
    """Authenticate or start a session"""
    if not CAS:
        return await Terminal(request, ADMINS[0]).start(request)
    ticket = request.url.path_qs.split('ticket=')[-1]
    if ticket:
        url = f'{CAS}/cas/validate?service={urllib.parse.quote(URL)}' \
              f'&ticket={urllib.parse.quote(ticket)}'
        async with aiohttp.ClientSession() as client:
            async with client.get(url) as response:
                if response.status == 200:
                    content = await response.text()
                    if content.startswith('yes\n'):
                        login = content.split('\n')[1].lower()
                        return await Terminal(request, login).start(request)
    service = f'{CAS}/login?service={urllib.parse.quote(URL.encode("ascii"))}'
    return web.HTTPFound(service)

async def data(request):
    """Analyse browser request"""
    post = await request.post()
    cookie = post.get('cookie', '')
    if cookie in Terminal.terminals:
        return await Terminal.terminals[cookie].data(request, post)
    return web.Response(body=b"bad")

class QuestionStat:
    """Record information about the question"""
    def __init__(self):
        self.start = time.time()
        self.logs = []
    def log(self, what, more=None):
        """Record a a time stamp"""
        if more:
            self.logs.append((int(time.time() - self.start), what, more))
        else:
            self.logs.append((int(time.time() - self.start), what))
    def __str__(self):
        return repr((int(self.start), self.logs))

class Terminal: # pylint: disable=too-many-instance-attributes,too-many-public-methods
    """Terminal emulator"""
    stream = None
    running = False
    running_question = False
    question = None
    question_stats = None
    question_done = True # No more display messages if question is done
    question_success = False
    next_question = None
    request = False
    warns = None
    starttime = 0
    expired_timers = set()
    screen_content = None
    screen = ''
    terminals = {} # Class variable
    master = slave = stdout = '!' # PTY
    selection = '' # Current selection
    silent = False
    screen_changed = True
    shell = None # Shell process

    def __init__(self, request, login, silent=False, compute_course=False):
        try:
            if request.path == '/erase':
                login = ADMINS[0]
                try:
                    os.unlink(f'LOGS/{ADMINS[0]}')
                except OSError:
                    pass
        except AttributeError:
            pass

        self.lock_get_screen = threading.Lock()
        terminal = self.from_login(login)
        if terminal and not compute_course:
            terminal.close() # Sansoori on 2 tabs if forbiden

        random.seed(id(request) + id(login) + id(self))
        self.cookie = hex(random.randrange(1<<64))[2:]
        self.terminals[self.cookie] = self
        self.login = login
        self.uid = pwd.getpwnam(self.login).pw_uid
        self.questions = core.Questions(
            f'LOGS/{login}', f'{config.SANSOORI_HOME}/{login}', not compute_course)
        self.home = self.questions.home
        self.lock_get_screen.acquire()
        try:
            self.event_loop = asyncio.get_running_loop()
        except RuntimeError:
            pass
        self.silent = silent
        self.compute_course = compute_course
        self.debug = (self.compute_course
            or (config.SANSOORI_IP == '127.0.0.1'
                and not config.SANSOORI_CAS
                and request != 'request' # compute courses or svg
                and request.path != '/erase' # Screen shots
               )
            )
        self.is_admin = login in ADMINS
        for name in sys.argv[1:]:
            self.next_question = self.questions.questions_dict.get(
                name.split('/')[-1].replace('.py', ''), None)
            if self.next_question:
                sys.argv.remove(name)
                break
        self.courses = []
        self.log('INIT')

    def from_login(self, login):
        """Return the terminal of this login"""
        for terminal in tuple(self.terminals.values()):
            if terminal.login == login:
                return terminal
        return None

    def log(self, txt):
        """Log state"""
        if self.silent:
            return
        try:
            nr_tasks = len(asyncio.all_tasks())
        except RuntimeError:
            nr_tasks = '?'
        print(f"{id(self)} T{nr_tasks}R{int(self.running)}r{int(self.running_question)}"
              f"m{self.master}s{self.slave}C{int(self.screen_changed)}"
              f"S{int(self.lock_get_screen.locked())} {self.login} {txt}")

    async def stdin(self, post):
        """Get stdin content"""
        if not self.running:
            return web.Response(body=b'bad')
        key_data = post['data'].replace('\r\n', '\n').encode('utf-8')
        self.log(f"STDIN {repr(key_data)[:80]}")
        while True:
            try:
                if self.master != '?':
                    os.write(self.master, key_data)
                break
            except BlockingIOError:
                await asyncio.sleep(0.1)
        return web.Response(body=b'ok')

    async def store_screen(self, post):
        """Get the screen content from browser"""
        self.screen = post['data'].replace('\r\n', '\n')
        self.log(f"SCREEN {repr(self.screen)[:30]} ||||||| {repr(self.screen)[-30:]}")
        try:
            self.lock_get_screen.release()
        except RuntimeError:
            # Unexpected screen receive (scroll)
            pass
        return web.Response(body=b'ok')

    async def goto_question(self, short_name=None):
        """Launch the question"""
        if short_name:
            self.next_question = self.questions.questions_dict[short_name]
        self.running_question = False
        await self.run_stop_question() # Will unlock readers

    async def reload(self):
        """Reload modified questions"""
        error = self.questions.update_questionnary()
        if error != 'Reload success':
            await self.stream.write(
                f'alert({json.dumps(error)});'.encode('utf-8'))
        # Redo the same question
        await self.goto_question(self.question.short_name)
        self.update_course()

    async def save_question_source(self, post):
        """Save the new source for the question"""
        source = post['data'].replace('\r\n', '\n')
        self.log(f"SAVE {repr(source)[:80]}")
        with open(self.question.filename, "w", encoding='utf-8') as file:
            file.write(source)
        await self.reload()

    async def data(self, request, post): # pylint: disable=too-many-return-statements
        """Get data"""
        action = post['action']
        if action == 'stdin':
            return await self.stdin(post)
        if action == 'screen':
            return await self.store_screen(post)
        if action == 'stdout':
            self.request = request
            return await self.run()
        if action == 'students' and self.is_admin:
            await self.stream.write(await self.students())
            return web.Response(body=b'ok')
        if action == 'details' and self.is_admin:
            return await self.details(post['data'])
        # Next ones will return ok
        if action == 'save' and self.is_admin:
            await self.save_question_source(post)
        elif action == 'selection':
            self.selection = post['data'].replace('\r\n', '\n')
        elif action == 'comment':
            with open('comments.py', 'a', encoding='utf-8') as file:
                file.write(repr((
                    int(time.time()),
                    self.login,
                    self.question.name if self.question else '?',
                    post['data'].replace('\r\n', '\n')
                    )) + '\n')
        elif action == 'graph':
            svg = await self.graph()
            svg = json.dumps(svg.decode('utf-8'))
            await self.stream.write(f'svg({svg});'.encode('utf-8'))
        elif action == 'debug':
            self.debug = 1 - int(self.debug)
        elif action == 'course':
            await self.stream.write('window.open("xxx-course.html");'.encode('utf-8'))
        elif action == 'edit' and self.is_admin:
            self.question_stats.log('Edit')
            with open(self.question.filename, 'r', encoding='utf-8') as file:
                content = file.read()
            self.send_source(content)
        elif action == 'reload' and self.is_admin:
            self.question_stats.log('Reload')
            await self.reload()
        elif action == 'goto':
            where = post['data']
            if where in self.questions.questions_dict:
                await self.goto_question(where)
        elif action == 'quit':
            await self.stream.write(
                'box("Au plaisir de vous revoir !", box_class="end");'.encode('utf-8'))
            self.running = False
            self.running_question = False
        elif action == 'next':
            await self.goto_question()
        else:
            return web.Response(body=b'bad')
        return web.Response(body=b'ok')

    async def students(self):
        """Student list"""
        self.log("STUDENTS")
        content = []
        for terminal in self.terminals.values():
            infos = [terminal.login]
            if terminal.question:
                infos.append(terminal.question.short_name)
            else:
                infos.append("?")
            content.append(infos)
        return f'display_students({json.dumps(content)});'.encode('utf-8')

    async def details(self, logins):
        """Display student details"""
        self.log(f"DETAILS {logins}")
        infos = {}
        for login in logins.split(' '):
            terminal = self.from_login(login)
            if terminal:
                screen = terminal.screen
            else:
                screen = 'no screen'
            if terminal.question:
                question = terminal.question.short_name
            else:
                question = "?"
            infos[login] = [question, screen]
        return web.Response(body=json.dumps(infos).encode('utf-8'))

    async def run_init_session(self):
        """Initialise a questions session"""
        self.log("RUN")
        self.stream = web.StreamResponse()
        self.stream.content_type = 'application/javascript'
        self.stream.headers['Cache-Control'] = 'no-store'
        await self.stream.prepare(self.request)
        # XXX Should be updated on reload
        competences = {key: value.value
                       for key, value in self.questions.competences.items()
                      }
        await self.stream.write(f'set_course({json.dumps(self.course_tree)});'
            .encode('utf-8'))
        await self.stream.write(f'set_competences({json.dumps(competences)});'
            .encode('utf-8'))
        if self.is_admin:
            await self.stream.write(b'is_admin();')

    async def run_create_pty(self):
        """Create the PTY"""
        self.master, self.slave = pty.openpty()
        self.log(f"MASTER {self.master} SLAVE {self.slave}")
        reader = asyncio.StreamReader()
        reader_protocol = asyncio.StreamReaderProtocol(reader)
        self.stdout = os.fdopen(self.master, 'rb')
        transport, _ = await self.event_loop.connect_read_pipe(lambda: reader_protocol, self.stdout)
        return transport, reader

    async def run_start_question(self): # pylint: disable=too-many-statements
        """Launch the question"""
        if self.next_question:
            question = self.next_question
            self.next_question = None
        else:
            question = self.questions.choose_question()
        self.question = question
        self.question_stats = QuestionStat()

        self.log(f"QUESTION {question.name} {self.questions.home}")
        command = self.questions.bash_command(self.login, question)
        self.shell = await asyncio.create_subprocess_shell(
            command,
            stdout=self.slave,
            stdin=self.slave,
            stderr=self.slave,
            close_fds=True,
            #start_new_session=True,
            )
        await self.stream.write(f'next_question({json.dumps(question.short_name)});'.encode('utf-8'))
        os.write(self.slave, b"\n"*23) # Prompt at the screen bottom
        self.log(f"SHELL CREATED with {repr(command)[:60]}")

        def analyser(): # pylint: disable=too-many-statements
            """Analyse student work IN A THREAD SO UNSAFE"""
            self.log("QUESTION RUN")
            self.reset_timer(True)
            self.question_done = False
            self.question_success = False
            try:
                question.run(self)
                self.questions.log_success(self.question)
                self.print('Bravo !', box_class='good')
                self.question_stats.log('Good')
                self.question_success = True
            except StopQuestion:
                self.log('QUESTION STOPPED BY SYSTEM')
                self.giveup()
            except (ConnectionClose, Cheat, Fail) as error:
                self.giveup()
                if isinstance(error, ConnectionClose):
                    self.question_stats.log('Close')
                    self.questions.log_close(self.question)
                    self.transport.close()
                    try:
                        os.close(self.slave)
                    except OSError:
                        pass
                    self.running = False
                elif isinstance(error, Cheat):
                    self.question_stats.log('Cheat')
                    self.questions.log_cheat(self.question)
                    self.print("Désolé, vous n'avez pas respecté les consignes. "
                        + error.args[0], box_class="cheat")
                elif isinstance(error, Fail):
                    self.question_stats.log('False')
                    self.questions.log_fail(self.question)
                    self.print("Désolé, vous avez échoué. " + error.args[0], box_class="fail")
                else:
                    raise
            except: # pylint: disable=bare-except
                self.print("Vous êtes tombé sur un bug, prévenez l'enseignant."
                + ('<pre>' + html.escape(traceback.format_exc()) + '</pre>'
                   if self.debug else ''), box_class="bug")
                self.questions.log_close(self.question)
                self.log(f"BUG {self.question.name}\n" + traceback.format_exc())
            self.log(f"QUESTION DONE {self.questions.updated_competences}")
            # XXX possible race ?
            with open(f'STATS/{self.question.short_name}', 'a', encoding='utf-8') as file:
                file.write(str(self.question_stats) + '\n')
            self.update_competences()
            self.question_done = True
            try:
                while self.running_question:
                    self.get_screen() # Let the student continue to launch commands
            except ConnectionClose:
                pass
            self.log("QUESTION REALLY DONE")
            self.question_done = 'done'

        self.questions.updated_competences = set()
        self.event_loop.run_in_executor(None, analyser)
        self.running_question = True

    def update_competences(self):
        """Send the competence changes to browser"""
        competences = {
            competence: self.questions.competences[competence].value
            for competence in self.questions.updated_competences
            }
        self.log(f'COMPETENCES {competences}')
        if competences:
            self.send_raw(f'update_competences({json.dumps(competences)});')
            self.questions.updated_competences = set()

    def giveup(self):
        """Stop the current question and loose the competences"""
        self.log('GIVEUP')
        self.questions.giveup(self.question)
        self.running_question = False

    async def run_copy_shell_to_browser(self, reader):
        """"Send the shell output to the browser"""
        # os.system("ps -fle | grep rcfile")
        self.log('COPY SHELL TO BROWSER START')
        while self.running_question:
            shell_output = await reader.read(1000)
            if not shell_output:
                break
            self.log(f"STDOUT {repr(shell_output)[:80]}")
            to_send = json.dumps(shell_output.decode('utf-8').replace('\n', '\r\n'))
            await self.stream.write(f'stdout({to_send});'.encode('utf-8'))
            self.screen_changed = True
        self.log('COPY SHELL TO BROWSER STOP')

    async def run_stop_question(self):
        """Clean up state for a new question"""
        if self.master == '?':
            return # Yet stopped
        self.log("CLEANUP")
        self.stdout.close()
        if self.running:
            self.transport.close()
            os.close(self.slave)
        self.master = self.slave = '?'
        try:
            with open(f"/sys/fs/cgroup/SANSOORI_{self.uid}/cgroup.kill",
                      "w", encoding='ascii') as file:
                file.write("1\n")
            # shell.kill()
        except (ProcessLookupError, PermissionError):
            pass
        # os.system("ps -fle | grep rcfile")
        self.log(f'CLEANUP DONE SHELL EXIT {await self.shell.wait()}')

    async def start(self, request):
        """Return the initial HTML page with "cookie"."""
        self.log("START")
        mime, content = get_file(request)
        content = content.decode('utf-8')
        content = content.format(cookie=json.dumps(self.cookie)).encode('utf-8')
        return web.Response(body=content, content_type=mime)

    async def run(self):
        """Start terminal and run questions"""
        await self.run_init_session()
        self.running = True
        while self.running:
            self.screen = ''
            self.screen_changed = True
            self.transport, reader = await self.run_create_pty()
            await self.run_start_question()
            await self.update_status()
            await self.run_copy_shell_to_browser(reader) # Wait question done
            await self.run_stop_question()
            self.screen_changed = True # force get_screen failure
            while self.question_done != 'done':
                await asyncio.sleep(0.1)
        self.stream.force_close()
        self.terminals.pop(self.cookie, None)
        return self.stream

    async def update_status(self):
        """Update status bar in browser"""
        status = f'''<b>{self.question.short_name}</b>
        {self.login}
        <b>{self.questions.nr_competences()}</b><span style="font-family:emoji">🎖</span>
        '''
        if self.debug:
            status += f' {self.master}-{self.slave} {len(self.terminals)} '
        await self.stream.write(f'box({json.dumps(status)}, "status");'.encode('utf-8'))

    def close(self):
        """Force close"""
        self.log('CLOSE!!!!!!!!!!!!!!')
        try:
            self.request.transport.close()
        except AttributeError:
            pass # self.request.transport is None because yet closed
        self.terminals.pop(self.cookie)

    async def send_raw_real(self, javascript):
        """Send the javascript data to the browser"""
        await self.stream.write(javascript.encode('utf-8'))

    def send_raw(self, javascript):
        """Send the javascript data to the browser"""
        asyncio.run_coroutine_threadsafe(self.send_raw_real(javascript), self.event_loop)

    def send_source(self, source):
        """Send source code to editor"""
        self.send_raw(f'edit({json.dumps(source)});')

    def clear_boxes(self):
        """Erase boxes from browser"""
        self.send_raw('clear_boxes();')

    def print(self, text, box_class='box'):
        """Print question text"""
        if self.question_done:
            return
        self.log(f'PRINT {repr(text)[:80]}')
        if text in self.warns:
            self.log("YET PRINTED")
            return text
        if box_class == 'box':
            self.question_stats.log('Print')
        printed = text
        for word, replacement in self.questions.words.items():
            printed = printed.replace(word, replacement) # Use the WORDS dictionaries
        self.warns[text] = printed
        self.send_raw(f'box({json.dumps(printed)},"{box_class}");')
        return text

    def tip(self, wait_time, text):
        """A tip. Returns True only on first call"""
        if text not in self.warns and self.expired_timer(wait_time):
            self.question_stats.log('Tip', wait_time)
            self.print(text, box_class="tip")
            return True
        return False

    def course(self, path, text, wait=True):
        """'path' indicate where this course part must be stored."""
        if self.compute_course:
            self.courses.append((path, text, self.question.short_name))
        self.question_stats.log('Course')
        self.print(text, box_class="course")
        if wait and not self.compute_course:
            self.sleep(3 * (text.count('\n\n') + 1))
        return True

    def ask(self, text):
        """Ask the student to do the job."""
        self.question_stats.log('Ask')
        self.print(text, box_class="ask")
        return True

    def transient(self, text, display):
        """Message is displayed only if display is True"""
        if display == (text in self.warns):
            return False
        if display:
            self.log(f"TRANSIENT SHOW {text}")
            self.question_stats.log('T+', text)
        else:
            self.log(f"TRANSIENT HIDE {text}")
            self.question_stats.log('T-', text)
        if text in self.warns:
            # Hide transient
            self.send_raw(f'hide_transient({json.dumps(self.warns.pop(text))});')
            return False # Do not display message
        self.print(text, box_class="transient")
        return True

    def get_screen(self, merge_space=False):
        """Get screen content"""
        if self.compute_course:
            return ''
        if not self.running_question:
            raise StopQuestion()
        # if self.expired_timer(120):
        #     self.print("Lancez la commande «exit» pour abandonner cette question.",
        #         box_class="giveup_tip")
        if not self.screen_changed:
            time.sleep(0.1)
            if not self.request.transport:
                raise ConnectionClose()
            return self.screen
        self.log("GET SCREEN START")
        self.screen_changed = False
        #print(dir(self.request.transport))
        self.send_raw('get_screen();')
        self.lock_get_screen.acquire()
        self.log("GET SCREEN DONE")
        if merge_space:
            self.screen = re.sub(' +', ' ', self.screen)
        last_line = self.screen.strip().rsplit('\n')[-1]
        self.transient("<«»>", '«' in last_line or '»' in last_line)
        return self.screen

    def until(self, merge_space=True, **kargs): # pylint: disable=too-many-branches
        """Returns screens until one contains the indicated text"""
        if self.compute_course:
            return # Stop loop
        self.question_stats.log('Until', ' '.join(kargs))
        self.reset_timer()
        get = test = None
        if len(kargs) > 1:
            raise ValueError("Only one test allowed")
        key, expected = next(iter(kargs.items()))
        if 'screen' in key:
            get = self.get_screen
        elif 'last_command' in key:
            get = self.get_last_command
        elif 'next_command' in key:
            get = self.get_next_command
        elif 'command' in key:
            get = self.get_command
        elif 'selection' in key:
            get = lambda merge_space: self.selection
        if '_contains_all' in key:
            def contains_all(value):
                for i in expected: # pylint: disable=cell-var-from-loop
                    if i not in value:
                        return False
                return True
            test = contains_all
        elif '_contains_one' in key:
            def contains_one(value):
                for i in expected: # pylint: disable=cell-var-from-loop
                    if i in value:
                        return True
                return False
            test = contains_one
        elif '_contains' in key:
            test = lambda value: expected in value # pylint: disable=cell-var-from-loop
        elif '_is' in key:
            test = lambda value: expected == value # pylint: disable=cell-var-from-loop
        elif '_startswith' in key:
            test = lambda value: value.startswith(expected) # pylint: disable=cell-var-from-loop
        elif '_endswith' in key:
            test = lambda value: value.endswith(expected) # pylint: disable=cell-var-from-loop
        if get and test:
            if key.startswith('not_'):
                full_test = lambda value: not test(value)
            else:
                full_test = test
        else:
            raise ValueError(
                "(not_)?{screen|(last_|next_|)command}"
                "_{is|contains|contains_all|contains_one|startswith|endswith}=string")
        while True:
            value = get(merge_space)
            if full_test(value):
                break
            yield value

    def get_commands(self, merge_space=True):
        """Get all the command lines on screen."""
        return [line[2:]
                for line in self.get_screen(merge_space=merge_space).split('\n')
                if line.startswith('$')
               ]

    def get_last_command(self, merge_space=True):
        """Get the last command launched"""
        commands = self.get_commands(merge_space=merge_space)
        if len(commands) <= 1:
            return ''
        return commands[-2]

    def get_next_command(self, merge_space=True):
        """Get the command being edited"""
        commands = self.get_commands(merge_space=merge_space)
        if commands:
            return commands[-1]
        return ''

    def get_command(self, merge_space=True):
        """Get the current or last command"""
        return (self.get_next_command(merge_space=merge_space)
             or self.get_last_command(merge_space=merge_space))

    def expired_timer(self, alarm_time):
        """Return True if expired (only once)"""
        if alarm_time not in self.expired_timers:
            delta = time.time() - self.starttime
            if self.debug:
                delta *= 10
            if delta >= alarm_time:
                self.expired_timers.add(alarm_time)
                return True
        return False

    def reset_timer(self, reset_warns=False):
        """Reset timer"""
        self.starttime = time.time()
        self.expired_timers = set()
        if reset_warns:
            self.warns = {}
        self.log("RESET TIMER")

    def sleep(self, seconds):
        """Here to not import time"""
        if self.debug:
            seconds /= 10
        time.sleep(seconds)

    def expect(self, cmd_competence):
        """Wait the good command with a space after"""
        assert cmd_competence.startswith('CMD_')
        cmd = cmd_competence[4:]
        tip = self.questions.words.get(f'<{cmd_competence}>',
            f'Vous devez utiliser la commande <cmd>{cmd}</cmd>')
        for command in self.until(command_startswith=cmd):
            if self.tip(20, tip):
                self.mul(cmd_competence, 0.9)
        for command in self.until(command_startswith=cmd + ' '):
            if self.transient('<SPACE>', len(command) > len(cmd) and command[len(cmd)] != ' '):
                self.mul('argument', 0.9)
            if cmd + '\n' in self.get_screen():
                break
    def press_enter(self, **kargs):
        """Wait enter"""
        for _ in self.until(**kargs):
            self.tip(3, "<ENTER>")

    def wait(self, **kargs):
        """Wait the condition"""
        for _ in self.until(**kargs):
            pass

    @staticmethod
    def choice(items):
        """Return a random item from the list"""
        return random.choice(items)

    @staticmethod
    def cheat(message):
        """The student cheated"""
        raise Cheat(message)

    @staticmethod
    def fail(message):
        """The student failed"""
        raise Fail(message)

    def mul(self, competence, factor):
        """proxy"""
        self.questions.mul(competence, factor)

    def add(self, competence, value):
        """proxy"""
        self.questions.add(competence, value)

    async def graph(self):
        """Display the graph of competences and questions"""
        content = ['digraph "Sansoori" { node [shape="plain"];']
        for question in self.questions.questions:
            name = question.short_name
            success = self.questions.success[question.name]
            fail = self.questions.fail[question.name]
            content.append(f'Q{name} [label="{name}\\n+{success} -{fail}"];')
        content.append('node [shape="box";margin=0;style=filled];')
        for competence, infos in self.questions.competences.items():
            if infos.value > 8:
                color = '#00FF00'
            elif infos.value > 4:
                color = '#70FF70'
            elif infos.value > 2:
                color = '#B0FFB0'
            elif infos.value >= 1:
                color = '#E0FFE0'
            else:
                color = '#FFFFFF'
            content.append(
                f'C{competence} [label="{competence}\\n{infos.value:.1f}";fillcolor="{color}"];')
        for question in self.questions.questions:
            for competence in question.needs:
                content.append(f"C{competence} -> Q{question.short_name};")
            for competence in question.provides:
                content.append(f"Q{question.short_name} -> C{competence};")
        content.append('}')
        with open(f'LOGS/{self.login}.dot', "w", encoding="utf8") as file:
            file.write('\n'.join(content))
        dot = await asyncio.create_subprocess_shell(
            f"dot -Tsvg LOGS/{self.login}.dot",
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)
        shell_stdout, _stderr = await dot.communicate()
        return shell_stdout

    @classmethod
    def update_course(cls, login=os.getlogin()):
        """Create the course tree"""
        session = Terminal("request", login, silent=True, compute_course=True)
        session.home.mkdir(exist_ok=True)
        session.reset_timer(True)
        for question in sorted(session.questions.questions, key=lambda x: x.depth):
            session.question = question
            session.questions.bash_command('doc', question) # Must call question.INIT()
            session.question_stats = QuestionStat()
            question.run(session)
            print(question.short_name, end=' ', file=sys.stderr, flush=True)
        print(file=sys.stderr)
        index = {}
        items = []
        for i, item in enumerate(session.courses):
            if item[0] not in index:
                index[item[0]] = i
            question = session.questions.questions_dict[item[2]]
            items.append(item + (question.needs, question.provides))
        sorted_items = sorted(items, key=lambda x: (x[0].split('/')[0], index[x[0]]))
        questions = iter(sorted_items)
        course = []

        def create(item, course):
            """Returns an upper tree item not storable here"""
            node = []
            course.append(node)
            where = item[0]
            while True:
                if item[0] == where:
                    node.append(item)
                    item = next(questions)
                    continue
                if not item[0].startswith(where):
                    return item # Not inside me
                item = create(item, node)
        try:
            item = next(questions)
            while item:
                item = create(item, course)
        except StopIteration:
            pass

        cls.course_tree = course

Terminal.update_course()

if 'graph' in sys.argv:
    async def print_graph(login):
        """Print graph"""
        svg = await Terminal("request", login, silent=True).graph()
        print(svg.decode('utf-8'))
    asyncio.run(print_graph(sys.argv[2]))
    sys.exit(0)

if 'compute_course' in sys.argv:
    def do_compute_course():
        """Compute course"""
        print('<style>')
        with open('sansoori.css', 'r', encoding='utf-8') as file:
            print(file.read())
        print('''
            BODY { max-width: 800px; text-align: justify; font-family: sans-serif;
                   font-size: 100%; margin-left: 0.5em; background: #FFF; }
            QL { position: absolute; left: 820px; margin-top: -1em;
                 background: #EEE; color: #888; font-size: 70%; line-height: 1.1em }
            </style>
            <link REL="icon" HREF="favicon.ico">
            <title>Support de cours</title>
            ''')
        def display(items, level=1):
            """Display course tree"""
            first = True
            for item in items:
                if isinstance(item, list):
                    print(f'<h{level}>{item[0][0]}</h{level}>')
                    print('<div style="margin-left: 2em">')
                    display(item, level+1)
                    print('</div>')
                else:
                    _where, course, question, needs, provides = item
                    if first:
                        first = False
                    else:
                        print('<hr>')
                    print(course)
                    needs = ' '.join(needs)
                    provides = ' '.join(provides)
                    print(f"<QL>{needs} →<br><b>{question}</b><br>→ {provides}</QL>")
        display(Terminal.course_tree)
        print(file=sys.stderr)

    do_compute_course()
    sys.exit(0)

if __name__ == '__main__':
    APP = web.Application()
    APP.add_routes([
        web.get('/', run),
        web.get('/erase', run),
        web.get("/node_modules/xterm/css/xterm.css", load),
        web.get("/node_modules/xterm/lib/xterm.js", load),
        web.get("/sansoori.html", load),
        web.get("/sansoori.css", load),
        web.get("/sansoori.js", load),
        web.get("/xxx-course.html", load),
        web.get("/xxx-graph.svg", load),
        web.get("/favicon.ico", load),
        web.get("/index.html", load),
        web.get("/xxx-1-start.png", load),
        web.get("/xxx-2-dot-grey.png", load),
        web.get("/xxx-3-more.png", load),
        web.get("/xxx-4-dot-blue.png", load),
        web.get("/xxx-5-ask.png", load),
        web.get("/xxx-6-good.png", load),
        web.get('/xxx-7-menu.png', load),
        web.get('/xxx-8-submenu.png', load),
        web.get('/xxx-9-subsubmenu.png', load),
        web.post("/data", data),
        ])
    web.run_app(APP, host=config.SANSOORI_IP, port=config.SANSOORI_PORT)
