#!/usr/bin/python3
"""
Global rename
"""

import os
import sys
import pathlib

if len(sys.argv) != 3:
    print(f"{sys.argv[0]} OldCompetenceName NewCompetenceName", file=sys.stderr)
    sys.exit(1)

old = sys.argv[1]
new = sys.argv[2]

def replace(line):
    """Protected string replacement"""
    return line.replace(f"'{old}'", f"'{new}'").replace(f'"{old}"', f'"{new}"')

def replace_in_questions(dirname):
    """Replace competence in questionnary"""
    for filename in os.listdir(dirname):
        if not filename.endswith('.py'):
            continue
        filename = f"{dirname}/{filename}"
        with open(filename, "r", encoding="utf-8") as file:
            lines = file.readlines()
        nr_changes = 0
        for i, line in enumerate(lines):
            if 'NEEDS' in line or 'PROVIDES' in line or '.mul(' in line:
                new_line = replace(line)
                if new_line != line:
                    nr_changes += 1
                    lines[i] = new_line
        if nr_changes:
            print(f"{nr_changes} changes in «{filename}»")
            with open(filename, "w", encoding="utf-8") as file:
                file.write(''.join(lines))

def replace_in_log():
    """Replace competence in logs"""
    for filename in pathlib.Path('LOGS').iterdir():
        if '.' in filename.name:
            continue
        with open(filename, "r", encoding="utf-8") as file:
            lines = file.readlines()
        nr_changes = 0
        for i, line in enumerate(lines):
            for action in ('add', 'mul'):
                line = line.replace(f", '{action}', '{old}', ", f", '{action}', '{new}', ")
            if line != lines[i]:
                nr_changes += 1
                lines[i] = line
        if nr_changes:
            print(f"{nr_changes} changes in «{filename}»")
            with open(filename, "w", encoding="utf-8") as file:
                file.write(''.join(lines))


replace_in_questions("QUESTIONS")
replace_in_log()
print("Changes done.")
