S = {
    status: undefined, // Element with status
    is_admin: false,
    menu_open: false, // Element containing the menu
    menu_buffer: '', // Path in the menu hierarchie
    good: false, // A good answer has been given for the current question
    nr_boxes: 0, // Full number of displayed boxes
    popup: undefined, // The popup window
    line: 9999, // For scrollback detection
    menu: {
        '²': ['Passer à la question suivante', function () { send('next') }],
        'e': ['Exprimer votre avis sur la question', comment_window],
        'a': ['Afficher...',
            {
                's': ['Le support de cours', function () { send('course') }],
                'm': ['Mes compétences', show_competences],
                'l': ['Légendes typographiques', function () { legend() }]
            }],
        'c': ['Parcourir le cours...'],
        'C': ['Parcourir les compétences...'],
        'i': ['Inverse gauche et droite', function () {
            document.body.className = document.body.className === '' ? 'left' : '';
            window.onresize();
        }],
        'Q': ['Quitter Sansoori', function () { send('quit') }]
    }
};

SPECIALS = ['xterm-scroll-area', 'status'];

var sansoori_url = window.location.toString().replace(/erase$/, '').split('?ticket')[0];

function log(x) {
    x.splice(0, 0, (new Date).getTime());
    console.log(x);
}

function send(action, data, callback) {
    log(['send', action, data]);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", sansoori_url + 'data', true);
    var form = new FormData();
    data ||= '';
    form.append('data', data.replace(/\r/g, '\n'));
    form.append('cookie', cookie);
    form.append('action', action);
    if (callback === undefined)
        callback = function (event) {
            if (event.target.response != 'ok') alert("Connexion perdue, rechargez la page");
        };
    xhr.onload = callback;
    xhr.send(form);
}

function students() {
    send('students');
}

function stdout_reader() {
    var stream = new XMLHttpRequest();
    stream.open("POST", 'data', true);
    stream.current_pos = 0;
    stream.onprogress = function (event) {
        var data = event.target.response.substr(event.target.current_pos);
        if (data.length < 80)
            log(['stdout', data]);
        else
            log(['stdout', data.substr(0, 40) + '......' + data.substr(data.length - 40)]);
        event.target.current_pos = event.target.response.length;
        eval(data);
    };
    var form = new FormData();
    form.append('cookie', cookie);
    form.append('action', 'stdout');
    stream.send(form);
}

function get_menu_title(item) {
    if (item.title)
        return item.title;
    if (item[0] instanceof Array)
        return get_menu_title(item[0]);
    return item[0];
}

function menu_fill(level, menu) {
    var div = MENU.childNodes[level];
    div.className = 'visible';
    if (div.previousSibling)
        div.previousSibling.className += ' upper';
    if (div.childNodes.length)
        return; // Yet filled
    for (var key in menu) {
        var item = document.createElement('DIV');
        item.innerHTML = '<span>' + key + '</span><what>' + html(get_menu_title(menu[key])) + '</what>';
        item.key = key;
        if (menu[key][1]) {
            if (!(menu[key][1] instanceof Function))
                item.style.fontWeight = 'bold';
        }
        else
            item.style.color = "#F00"; // Bug
        if (menu[key][2] && !menu[key][2]())
            item.style.color = '#888';
        var y = menu[key][3];
        if (y > 0) {
            if (y <= 1)
                y = 50 * y;
            else if (y >= 10)
                y = 100;
            else
                y = 50 * Math.log10(10 * y); // between 50 and 100
            item.firstChild.style.background = 'linear-gradient(0deg, #0F0 0% '
                + y + '%, #FF0 ' + y + '% 100%)';
        }
        item.setAttribute('long', menu[key][0].length > 40 ? '1' : '0');
        div.appendChild(item);
    }
}
function menu_content(execute) {
    var path = S.menu_buffer;
    var menu = S.menu;
    var level = 0;
    while (1) {
        menu_fill(level++, menu);
        var char = path[0];
        if (char == undefined) {
            menu_clear(level);
            return;
        }
        var item = menu[char];
        if (!item || (item[2] && !item[2]())) {
            // Bad selector
            S.menu_buffer = S.menu_buffer.substr(0, S.menu_buffer.length - 1);
            return;
        }
        for (var choice = MENU.childNodes[level - 1].firstChild;
            choice; choice = choice.nextSibling)
            if (choice.key == char)
                choice.className = 'selected';
            else
                choice.className = '';
        path = path.substr(1);
        if (item[1] && !(item[1] instanceof Function))
            menu = item[1];
        else if (path === '') {
            if (execute) {
                setTimeout(menu_close, 500);
                item[1]();
            }
            return;
        }
    }
}

function menu_clear(level) {
    for (var item = MENU.childNodes[level]; item; item = item.nextSibling) {
        item.innerHTML = '';
        item.className = '';
    }
}

function menu_close() {
    S.menu_open = false;
    menu_clear(0);
}

function menu(data) {
    function selected() {
        if (S.menu_buffer.length == 0)
            return MENU.firstChild.firstChild;
        for (var item = MENU.childNodes[S.menu_buffer.length - 1].firstChild; item; item = item.nextSibling)
            if (item.className == 'selected')
                return item;
    }

    if (!S.menu_open) {
        S.menu_buffer = '';
        S.menu_open = true;
    }
    else
        switch (data) {
            case '\010':
            case '\177':
            case '\033[D':
                if (S.menu_buffer === '')
                    return menu_close();
                selected().className = '';
                S.menu_buffer = S.menu_buffer.substr(0, S.menu_buffer.length - 1);
                break;
            case '\033[C':
                for (var item = MENU.childNodes[S.menu_buffer.length].firstChild;
                    item;
                    item = item.nextSibling)
                    if (!item.style.color) {
                        S.menu_buffer += item.firstChild.textContent;
                        break;
                    }
                break;
            case '\033[A':
            case '\033[B':
                var item = selected();
                do {
                    if (data == '\033[A')
                        item = item.previousSibling;
                    else
                        item = item.nextSibling;
                }
                while (item && item.style.color);
                if (item) {
                    S.menu_buffer = S.menu_buffer.substr(0, S.menu_buffer.length - 1)
                        + item.firstChild.firstChild.textContent;
                    menu_clear(S.menu_buffer.length);
                }
                break;
            case '\033':
                return menu_close();
            case '\r':
                break;
            default:
                if (data.length == 1)
                    S.menu_buffer += data;
                if (S.menu_buffer === '' && data != '²')
                    return menu_close();
        }
    menu_content(data.length == 1);
}

function is_admin() {
    S.is_admin = true;
    S.menu['&'] = ['Administrateur Sansoori...',
        {
            'd': ['Debug mode toggle', function () { send('debug') }],
            'e': ['Editer le code source de la question', function () { send('edit') }],
            's': ['Surveiller les étudiants', students],
            'r': ['Recharger le code source de la question', function () { send('reload') }],
            'g': ['Graphe de compétences', function () { send('graph') }]
        }];

}

setTimeout(stdout_reader, 100);

function onData(data) {
    if (S.menu_open || data == '²')
        return menu(data);
    if (S.popup) {
        if (data == '\033')
            close_window();
    }
    else
        send('stdin', data);
}

// Only internal copy paste works

function onSelectionChange() {
    var selection = term.getSelection();
    if (selection)
        term.the_last_selection = selection;
    send('selection', selection);
}

function customKeyEventHandler(event) {
    if (event.type === 'mouseup' && event.button === 1) {
        onData(term.the_last_selection);
        event.stopPropagation();
        return false; // Prevent default behavior
    }
    else if (event.type === 'paste') {
        event.stopPropagation();
        return false; // Prevent default behavior
    }
    return true; // Allow default behavior for other key events
}

function get_screen_real() {
    var lines = [];
    var i;
    for (i = term.buffer.active.viewportY; i < term.buffer.active.viewportY + term.rows; i++) {
        var xline = term.buffer.active.getLine(i);
        var line = xline.translateToString().trim();
        if (i - term.buffer.active.viewportY == term.buffer.active.cursorY) // Cursor on this line
            while (line.length < term.buffer.active.cursorX)
                line += ' '; // Add spaces before cursor
        if (xline.isWrapped)
            lines[lines.length - 1] += line;
        else
            lines.push(line);
    }
    while (lines[lines.length - 1] == '')
        lines.pop();
    send('screen', lines.join('\n'));
}

function get_screen() {
    term.write('', get_screen_real);
}

function next_question(question) {
    log(['next_question']);
    close_window();
    if (S.nr_boxes && !S.good && question != S.question)
        open_window("<h1>Vous avez perdu les compétences liées à la question que vous étiez en train de faire.</h1>");
    S.question = question;
    term.reset();
    clear_boxes();
}

function clear_boxes() {
    for (var i = 0; i < boxesElement.childNodes.length; i++) {
        var elm = boxesElement.childNodes[i];
        if (elm.className != 'status') {
            boxesElement.removeChild(elm);
            i--;
        }
    }
}

function hide_transient(text) {
    for (var i = boxesElement.childNodes.length - 1; i >= 0; i--)
        if (boxesElement.childNodes[i].innerHTML == text)
            boxesElement.removeChild(boxesElement.childNodes[i]);
}

var term = new Terminal(
    {
        background: "#888"
    });
term.open(document.getElementById('terminal'));
const viewportElement = term.element.querySelector('.xterm-viewport');
const screenElement = term.element.querySelector('.xterm-screen');
term.onData(onData);
term.onScroll(function (line) {
    if (line < S.line)
        get_screen_real();
    S.line = line;
});
term.onSelectionChange(onSelectionChange);
term.element.addEventListener('mouseup', customKeyEventHandler);
term.element.addEventListener('paste', customKeyEventHandler, { capture: true, useCapture: true });
term.focus();
const boxesElement = document.getElementById('boxes');
MENU = document.getElementById('menu');

window.onresize = function () {
    if (document.body.className === '')
        term.options.fontSize = Math.floor(1.3 * window.innerWidth / 80);
    else
        term.options.fontSize = Math.floor(1.15 * window.innerWidth / 80);
    // If window is smaller, the cursor may make appear a scrollbar.
    term.element.querySelector('.xterm-helper-textarea').style = '';
}
window.onresize();

window.onkeydown = function (event) {
    if (event.key == 'OS' || event.key == 'Meta') {
        onData('²');
        return;
    }
    if (S.popup && event.key == 'Escape') {
        close_window();
        return;
    }
    if (event.key != 'Control')
        return;
    for (var element = boxesElement.firstChild; element; element = element.nextSibling)
        if (element.getAttribute('closed') == '1') {
            element.setAttribute('closed', '0');
            break;
        }
    box_scroll();
};

function box_scroll() {
    var pos;
    if (document.body.className == 'left')
        pos = screenElement.offsetHeight;
    else
        pos = (24 / 25) * screenElement.offsetHeight;
    boxesElement.style.top = 100 * (pos - boxesElement.offsetHeight) / window.innerHeight + 'vh';
}

function box(text, box_class) {
    log(['box', box_class]);
    S.nr_boxes++;
    term.write('', function () { box_real(text, box_class); });
    setTimeout(box_scroll, 100);
}

function box_real(text, box_class) {
    var element;
    log(['box_real', box_class]);
    S.good = box_class == 'good';
    if (box_class == 'status') {
        if (S.status) {
            S.status.innerHTML = text;
            return;
        }
        element = S.status = document.createElement('BOX');
    }
    else
        element = document.createElement('BOX');
    element.className = box_class;
    if (box_class != 'status'
        && box_class != 'transient'
        && boxesElement.childNodes.length > 1)
        element.setAttribute('closed', '1');
    if (box_class == 'course' || box_class == 'ask' || box_class == 'box'
        || box_class == 'end' || box_class == 'giveup' || box_class == 'fail' || box_class == 'good')
        for (var e = boxesElement.firstChild; e; e = e.nextSibling)
            if (e.getAttribute('closed') == '1'
                && (e.className == 'tip' || e.className == 'giveup_tip')) {
                // Hide no more necessary tips
                var previous = e.previousSibling;
                e.parentNode.removeChild(e);
                e = previous;
            }
    // console.log(JSON.stringify(text));
    element.innerHTML = text.replace(/\n\n/g, '<p>');
    boxesElement.appendChild(element);
    if (box_class == 'end' || box_class == 'giveup' || box_class == 'fail' || box_class == 'good')
        for (var e = boxesElement.firstChild; e; e = e.nextSibling)
            e.setAttribute('closed', '0');
    else if (box_class == 'ask') {
        var first_ask = true;
        for (var e = boxesElement.firstChild; e; e = e.nextSibling) {
            if (e.getAttribute('closed') == '1') {
                // Display automaticaly the first closed box
                if (!first_ask)
                    e.setAttribute('closed', '0');
                break;
            }
            if (e.className == 'ask')
                first_ask = false;
        }
    }
    if (SPECIALS.includes(element.className))
        return;
}

function stdout(text) {
    term.write(text);
}

function open_window(content) {
    if (S.popup)
        close_window();
    S.popup = document.createElement("DIV");
    S.popup.className = 'popup';
    S.popup.innerHTML = content;
    var close = document.createElement("BUTTON");
    close.className = 'close';
    close.innerHTML = '<key>Esc</key> <key>Escape</key> <key>Ech</key> …<br>pour fermer la popup.';
    close.onclick = close_window;
    S.popup.appendChild(close);

    document.body.appendChild(S.popup);
}

function close_window() {
    if (!S.popup)
        return;
    document.body.removeChild(S.popup);
    S.popup = undefined;
    term.focus();
}

function comment_window() {
    open_window(
        '<div style="font-family: sans-serif; font-size: 2vw; line-height: 1.5em; margin:1em">Il y a un problème avec cette question ?<br>'
        + "Expliquez-le ici en quelques mots :"
        + '<textarea id="comment" style="font-size:100%; margin: 1em;width: calc(100% - 2em); height: 10em"></textarea>'
        + '<button onclick="send(\'comment\', document.getElementById(\'comment\').value);close_window();">Envoyer votre commentaire.</button></div>'
    );
    document.getElementById('comment').focus();
}

function svg(content) {
    open_window('<style>svg { width: 100% }</style>' + content);
    setTimeout(function () { S.popup.focus() }, 100);
}

var SOURCE;

function source_save() {
    var selection = getSelection();
    selection.selectAllChildren(SOURCE);
    send('save', selection.toString());
}

function source_key(event) {
    if (event.key == 's' && event.ctrlKey) {
        source_save();
        event.stopPropagation();
        event.preventDefault();
    }
    else if (event.key == 'Escape')
        close_window();
}

function edit(source) {
    open_window(
        '<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.7.0/build/styles/vs.min.css">'
        + '<style>#source { width: 100%; margin: 0px; min-height: 100%; }</style>'
        + '<PRE id="source" spellcheck="false" class="language-python" contenteditable>' + html(source) + '</PRE>');

    SOURCE = document.getElementById('source');
    SOURCE.onkeydown = source_key;
    SOURCE.focus();

    var script = document.createElement('SCRIPT');
    script.src = "https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.7.0/build/highlight.min.js";
    script.onload = function () { hljs.highlightElement(SOURCE); };
    document.body.appendChild(script);
}

function student_details() {
    if (!document.getElementById('students'))
        return;
    setTimeout(student_details, 1000);
    var logins = [];
    var trs = document.getElementsByTagName('TR');
    for (var i in trs)
        if (trs[i].className === 'watch')
            logins.push(trs[i].cells[0].textContent);
    if (logins.length)
        send('details', logins.join(' '), details);
}

function details(event) {
    var data = JSON.parse(event.target.response);
    var content = [];
    for (var login in data)
        content.push(login + ' ' + data[login][0] + '<br>'
            + '<PRE>' + data[login][1] + '</PRE>');
    document.getElementById('students_details').innerHTML = content.join('');
}

function html(t) {
    return t.replace(/&/g, '&amp;').replace(/>/g, '&gt;').replace(/</g, '&lt;');
}

function display_students(content) {
    var table = ['<TABLE id="students" class="students">'];
    for (var i in content) {
        var infos = content[i];
        table.push(
            '<TR onclick="this.className = this.className===\'\' ? \'watch\' : \'\'" '
            + 'id="' + infos[0] + '">'
            + '<TD>' + infos[0] + '<TD>' + html(infos[1]) + '</TR>')
    }
    table.push('</TABLE>');
    table.push('<DIV id="students_details"></DIV>');
    open_window(table.join(''));
    student_details();
}

function legend() {
    open_window(
        "<style>"
        + ".popup {"
        + "font-size: 1vw;"
        + "padding: 1em;"
        + "line-height: 1.4em;"
        + "}"
        + "</style>"
        + "<h2>Les graphismes en fonction du sens</h2>"
        + "Une commande shell complexe :<br>"
        + "<cmd>grep '<regexp>^[a-z]*$</regexp>' <path>/etc/passwd</path> <path><var>$HOME</var>/PROJET/<pat>*.[ch]</pat></path></cmd>"
        + "<ul>"
        + "<li> Commande         : <cmd>pwd</cmd>"
        + "<li> Expression régulière : <regexp>^[a-z]*$</regexp>"
        + "<li> Chemin : <path>/etc/passwd</path>"
        + "<li> Variable et contenu de variable : <var>$HOME</var>"
        + "<li> Pattern : <pat>*.[ch]</pat>"
        + "<li> Mot étranger     : comme <lang>word</lang> ou <lang>a priori</lang>"
        + "<li> Définition       : <def>répertoire courant</def> (<lang>current directory</lang>)"
        + "<li> Touche clavier   : <key>Ctrl+C</key>"
        + "<li> Chose importante : <important>À lire absolument !</important>"
        + "</ul>"
    );
}

function set_competences(competences) {
    S.competences = competences;
    update_competences({});
}

function update_competences(competences) {
    for (var competence in competences)
        S.competences[competence] = competences[competence];
    update_course();

    function create_tree(items) {
        var starts = {};
        for (var i in items) {
            var key = items[i][1].split('_')[0];
            if (starts[key] === undefined)
                starts[key] = [];
            items[i][1] = items[i][1].replace(/[^_]*_/, '');
            starts[key].push(items[i]);
        }
        var sorted = [];
        for (var i in starts)
            sorted.push(i);
        sorted.sort();
        var tree = [];
        for (var i in sorted) {
            if (starts[sorted[i]].length == 1)
                tree.push(starts[sorted[i]][0]);
            else {
                var child_tree = create_tree(starts[sorted[i]]);
                child_tree.title = sorted[i];
                tree.push(child_tree);
            }
        }
        return tree;
    }
    var course = [];
    for (var i in S.competences) {
        course.push([i, i, i, [], [i]]);
    }
    course = create_tree(course);
    console.log("COMP");
    console.log(course);

    var menu = {};
    var value_and_weight = update_course(course, menu, create_leaf_competence);
    S.menu['C'] = [S.menu['C'][0], menu, undefined,
    value_and_weight[0], value_and_weight[1]];

}

function show_competences() {
    var content = [];
    for (i in S.competences)
        content.push('<tr><td>' + i + '<td>' + S.competences[i] + '</tr>');
    content.sort();
    content.splice(0, 0, '<table><tr><th>Code<th>Acquis si ≥ 1</tr>');
    content.push('</table>');
    open_window(content.join(''));
    setTimeout(function () { S.popup.focus() }, 100);
}

function set_course(course) {
    S.course = course;
    update_course();
}

function always_false() {
    return false;
}

function create_leaf_course(item) {
    let question_name = item[2];
    var min = 9999;
    var allowed = true;
    if (S.competences) {
        for (var j in item[4])
            if (S.competences[item[4][j]] < min)
                min = S.competences[item[4][j]];
        for (var j in item[3])
            if (!(S.competences[item[3][j]] > 0))
                allowed = false;
    }
    if (min == 9999)
        min = 0;
    if (allowed) {
        for (var j in item[4]) {
            var competence = item[4][j];
            if (!S.competence_to_question[competence])
                S.competence_to_question[competence] = {};
            S.competence_to_question[competence][question_name] = min;
        }
    }
    return [
        item[1].trim().replace(/<[^>]*>*/g, '').substr(0, 150),
        function () { send('goto', question_name); },
        allowed ? undefined : always_false, min, 1
    ];
}

function create_leaf_competence(item) {
    var questions = S.competence_to_question[item[0]];
    if (questions === undefined)
        return [item[1], , always_false, 0, 1];
    var min = 1e50, question;
    for (var i in questions) {
        if (questions[i] < min) {
            min = questions[i];
            question = i;
        }
    }
    return [item[1], function () { send('goto', question); }, , min, 1];
}

function update_course(course, menu, create_leaf) {
    if (!menu) {
        menu = {};
        S.competence_to_question = {};
        value_and_weight = update_course(S.course, menu, create_leaf_course);
        S.menu['c'] = [S.menu['c'][0], menu, undefined, value_and_weight[0], value_and_weight[1]];
        return;
    }
    var first = 'a'.charCodeAt(0);
    var sum = 0., nr = 0;
    for (var i = 0; i < course.length; i++) { // because of 'title' attributes
        // Leaf:
        //    [Course Path, Course Text, Question Name, Requires, Provides]
        // Node:
        //    [Leaf, Leaf, Node, Leaf...]
        var key = String.fromCharCode(first + Number(i));
        if (course[i][0] instanceof Array) { // Node
            var content = {};
            value_and_weight = update_course(course[i], content, create_leaf);
            menu[key] = [(course[i].title || course[i][0][0]).replace(/.*[:\/]/, ''),
                content, undefined, value_and_weight[0], value_and_weight[1]];
        }
        else // Leaf
            menu[key] = create_leaf(course[i]);
        sum += menu[key][3] * menu[key][4];
        nr += menu[key][4];
    }
    return [sum / nr, nr];
}